<!DOCTYPE HTML>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>{{ \setting('app_name', 'Sinar Matahari Kaca') }} | Toko Penjualan Segala Jenis Kaca | @yield('title')</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Penjualan dan Instalasi Alumunium, Besi, Kaca, Accessories dan Etalase" />
	<meta name="keywords" content="Jual Kaca, Toko Kaca, Instalasi Alumunium, Besi, Kaca, Accessories, Etalase, Etalase murah, Jual Kaca, Jual Besi, Jual Alumunium, Kaca Polos, Riben, Cermin, Cermin Biru, Cermin Flora, Cermin One Way, Jenis Ukuran Kaca, Jenis Ketebalan Kaca" />
	<meta name="author" content="https://sinarmataharikaca.com" />

  	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content=@yield('title')/>
	<meta property="og:image" content="https://sinarmataharikaca.com/frontend/images/about.jpg"/>
	<meta property="og:url" content="https://sinarmataharikaca.com"/>
	<meta property="og:site_name" content="Sinar Matahari Kaca"/>
	<meta property="og:description" content="Penjualan dan Instalasi Alumunium, Besi, Kaca, Accessories dan Etalase"/>
	<meta name="twitter:title" content=@yield('title') />
	<meta name="twitter:image" content="https://sinarmataharikaca.com/frontend/images/about.jpg" />
	<meta name="twitter:url" content="https://sinarmataharikaca.com" />
	<meta name="twitter:card" content="https://sinarmataharikaca.com" />

	<!-- <link href="https://fonts.googleapis.com/css?family=Droid+Sans" rel="stylesheet"> -->
	<link rel="shortcut icon" type="/image/png" href="/frontend/images/favicon.png"/>
	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
	<!-- Animate.css -->
	<link rel="stylesheet" href="/frontend/css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="/frontend/css/icomoon.css">
	<!-- Themify Icons-->
	<link rel="stylesheet" href="/frontend/css/themify-icons.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="/frontend/css/bootstrap.css">
	<!-- Magnific Popup -->
	<link rel="stylesheet" href="/frontend/css/magnific-popup.css">
	<!-- Owl Carousel  -->
	<link rel="stylesheet" href="/frontend/css/owl.carousel.min.css">
	<link rel="stylesheet" href="/frontend/css/owl.theme.default.min.css">
	<!-- Flexslider -->
	<link rel="stylesheet" href="/frontend/css/flexslider.css">
	<!-- Theme style  -->
	<link rel="stylesheet" href="/frontend/css/style.css">

	<link rel="stylesheet" href="/frontend/css/responsive.css">
	<link rel="stylesheet" href="/frontend/css/prettyPhoto.css">

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.15.6/styles/codepen-embed.min.css">

	<!-- Modernizr JS -->
	<script src="/frontend/js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="/frontend/js/respond.min.js"></script>
	<![endif]-->

	</head>
	<body>
		
	<div class="smk-loader"></div>
	
	<div id="page">
	<nav class="smk-nav" role="navigation">
		<div class="smk-container">
			<div class="row">
				<div class="col-sm-2 col-xs-12">
					<div id="smk-logo"><a href="{{ route('welcome') }}"><img src="/frontend/images/sinar-matahari-kaca-logo.png" width="200"/></a></div>
				</div>
				<div class="col-xs-10 text-right menu-1 main-nav">
					<ul>
						<li class="active"><a href="{{ route('welcome') }}" data-nav-section="home">Home</a></li>
						<li><a href="{{ route('welcome') }}" data-nav-section="blog">Blog</a></li>
						<li><a href="{{ route('welcome') }}" data-nav-section="services">Services</a></li>
						<li><a href="{{ route('welcome') }}" data-nav-section="product">Product</a></li>
                        <li><a href="{{ route('welcome') }}" data-nav-section="about">About us</a></li>
						<li><a href="{{ route('welcome') }}" data-nav-section="contact">Contact</a></li>
					</ul>
				</div>
			</div>
			
		</div>
    </nav>
    
    @yield('content')

	<footer id="smk-footer" role="contentinfo">
			<div class="smk-container">
				
				<div class="row copyright">
					<div class="col-md-12">
						<p class="pull-left">
							<small class="block">&copy; 2019 Sinar Matahari Kaca</small> 
						</p>
						<p class="pull-right">
							<ul class="smk-social-icons pull-right">
								@guest
								<li class="nav-item">
									<a href="{{ route('login') }}"><i class="fa fa-sign-in-alt"></i> {{ __('Login') }}</a>
								</li>
								@else
								<li class="nav-item">
									<a href="{{ route('home') }}"><i class="fa fa-tachometer-alt"></i> {{ __('Dashboard') }}</a>
								</li>
								@endguest
								<li><a href="{{ route('blog') }}"><i class="fa fa-blog"></i> Blog</a></li>
								<li><a href="{{ route('contact') }}"><i class="fas fa-paper-plane"></i> Direct Email</a></li>
								<li><a href="https://bit.ly/2O9mrkV"><i class="fab fa-whatsapp"></i> Direct Chat</a></li>
							</ul>
						</p>
					</div>
				</div>
	
			</div>
		</footer>
		</div>
	
		<div class="gototop js-top">
			<a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
		</div>
		
		<!-- jQuery -->
		<script src="/frontend/js/jquery.min.js"></script>
		<!-- jQuery Easing -->
		<script src="/frontend/js/jquery.easing.1.3.js"></script>
		<!-- Bootstrap -->
		<script src="/frontend/js/bootstrap.min.js"></script>
		<!-- Waypoints -->
		<script src="/frontend/js/jquery.waypoints.min.js"></script>
		<!-- Carousel -->
		<script src="/frontend/js/owl.carousel.min.js"></script>
		<!-- countTo -->
		<script src="/frontend/js/jquery.countTo.js"></script>
		<!-- Flexslider -->
		<script src="/frontend/js/jquery.flexslider-min.js"></script>
		<!-- Magnific Popup -->
		<script src="/frontend/js/jquery.magnific-popup.min.js"></script>
		<script src="/frontend/js/magnific-popup-options.js"></script>
		<!-- Main -->
		<script src="/frontend/js/main.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.15.6/highlight.min.js"></script>
		<script>hljs.initHighlightingOnLoad();</script>
	
		</body>
	</html>