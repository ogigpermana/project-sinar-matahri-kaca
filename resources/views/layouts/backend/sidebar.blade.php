<aside class="main-sidebar">
<!-- sidebar: style can be found in sidebar.less -->
<section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
    <div class="pull-left image">
        <img src="{{ Auth::user()->avatar() }}" class="img-circle" alt="User Image">
    </div>
    <div class="pull-left info">
        <p>{{ Auth::user()->name }}</p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
    </div>
    </div>

    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
    <li>
        <a href="{{ route('home') }}">
        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        </a>
    </li>
    <li class="treeview">
        <a href="#">
        <i class="fa fa-folder"></i>
        <span>Blog</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
        </a>
        <ul class="treeview-menu">
        <li><a href="{{ route('blog.index') }}"><i class="fa fa-list" style="color:orange"></i> Semua list artikel</a></li>
        <li><a href="{{ route('blog.create') }}"><i class="fa fa-pencil" style="color:orange"></i> Tambah artikel baru</a></li>
        </ul>
    </li>
    @if(check_user_permissions(request(), "Category@index"))
    <li class="treeview">
        <a href="#">
        <i class="fa fa-folder"></i>
        <span>Kategorisasi</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
        </a>
        <ul class="treeview-menu">
        <li><a href="{{ route('category.index') }}"><i class="fa fa-list" style="color:orange"></i> Semua Kategori</a></li>
        <li><a href="{{ route('category.create') }}"><i class="fa fa-pencil" style="color:orange"></i> Buat kategori baru</a></li>
        </ul>
    </li>
    @endif
    @if(check_user_permissions(request(), "User@index"))
    <li class="treeview">
        <a href="#">
        <i class="fa fa-folder"></i>
        <span>Pengguna / User</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
        </a>
        <ul class="treeview-menu">
        <li><a href="{{ route('user.index') }}"><i class="fa fa-users" style="color:orange"></i> Semua pengguna</a></li>
        <li><a href="{{ route('user.create') }}"><i class="fa fa-user-plus" style="color:orange"></i> Tambah pengguna baru</a></li>
        </ul>
    </li>
    @endif
    @if(check_user_permissions(request(), "Project@index"))
    <li><a href="{{ route('tag.index') }}"><i class="fa fa-tags"></i> <span>Tags</span></a></li>
    <li><a href="{{ route('comment.index') }}"><i class="fa fa-comments"></i> <span>Komentar</span></a></li>
    <li><a href="{{ route('gallery.index') }}"><i class="fa fa-image"></i> <span>Galeri Proyek</span></a></li>
    @endif
    <li><a href="{{ route('image.manager') }}"><i class="fa fa-file-image-o"></i> <span>Pengatur Gambar</span></a></li>
    <li><a href="{{ route('profile.edit') }}"><i class="fa fa-id-card"></i> <span>Edit profil</span></a></li>
    <li><a href="{{ route('form.change.password') }}"><i class="fa fa-unlock-alt"></i> <span>Ubah kata sandi</span></a></li>
    @if(check_user_permissions(request(), "Setting@index"))
    <li><a href="{{ route('settings') }}"><i class="fa fa-cog"></i> <span>Pengaturan aplikasi</span></a></li>
    @endif
    </ul>
</section>
<!-- /.sidebar -->
</aside>