<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 2.3.6
    </div>
    <strong>Copyright &copy; 2019 {{ \setting('app_name') }} | <strong>template</strong> by <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong>
</footer>