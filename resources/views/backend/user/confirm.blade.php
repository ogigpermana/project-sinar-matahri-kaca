@extends('layouts.backend.app')
@section('title', 'Hapus pengguna')
@section('content')
<body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
        
          <!--Header section -->
          @include('layouts.backend.navbar')
          <!-- Left side column. contains the logo and sidebar -->
          @include('layouts.backend.sidebar')
        
          <!-- Content Wrapper. Contains page content -->
          <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
              <h1>
                User
                <small>Konfirmasi penghapusan</small>
              </h1>
              <ol class="breadcrumb">
                <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="{{ route('user.index') }}">Users</a></li>
                <li class="active">Konfirmasi penghapusan</li>
              </ol>
            </section>
        
            <!-- Main content -->
            <section class="content">
                <div class="row">
                <!-- form start -->
                {!! Form::model($user, [
                  'method' => 'DELETE',
                  'route' => ['user.destroy', $user->id],
                  ]) !!}

                  @csrf
                  <div class="col-xs-12">
                    <div class="box box-info with-border">
                      <div class="box-body">
                          <p>Anda akan menghapus data user dibawah ini:</p>
                          <p>#ID {{ $user->id }} : <span class="badge badge-danger">{{ $user->name }}</span></p>
                          <p>Silahkan pilih opsi berikut:</p>
                          {{-- <p>
                              <input type="radio" name="delete_option" value="delete" checked>Hapus seluruh konten terkait user</p> --}}
                          <p>
                              <input type="radio" name="delete_option" value="attribute">Alihkan konten pada user pilihan dikanan : 
                          {!! Form::select('selected_user', $users, null) !!}
                          </p>
                      </div>
                      <div class="box-footer">
                          <button class="btn btn-danger" onclick="return confirm('Anda yakin mau menghapus data tersebut?')" type="submit">Konfirmasi hapus</button>
                          <a class="btn btn-default" href="{{ route('user.index') }}">Batal</a>
                      </div>
                    </div>
                  </div>

                {!! Form::close() !!}
                </div>
              <!-- ./row -->
            </section>
            <!-- /.content -->
          </div>
          <!-- /.content-wrapper -->
          @include('layouts.backend.foot-note')
        
        </div>
        <!-- ./wrapper -->
        @include('backend.user.partial.script')
@endsection
