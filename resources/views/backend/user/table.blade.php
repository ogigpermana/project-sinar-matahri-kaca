<table class="table table-hover table-bordered">
    <thead>
        <tr class="info">
            <th width="">Nama Pengguna</th>
            <th width="">Nama Unik</th>
            <th width="150">Email</th>
            <th width="150">User Level</th>
            <th width="140">Action</th>
        </tr>
    </thead>
    <tbody>
        @forelse($users as $user)
        <tr>
            <td>{{ $user->name }}</td>
            <td>{{ $user->slug }}</td>
            <td>{{ $user->email }}</td>
            <td>{{ $user->roles->first()->display_name }}</td>
            <td>
                <a class="btn btn-xs btn-primary" href="{{ route('user.edit', $user->id) }}"><i class="fa fa-pencil"></i> Edit</a>
                @if ($user->id == config('cms.default_user_id'))
                <button onclick="return false" type="submit" class="btn btn-xs btn-danger" disabled>
                    <i class="fa fa-times"></i> Delete
                </button>
                @else
                <a href="{{ route('user.confirm', $user->id) }}" type="submit" class="btn btn-xs btn-danger">
                    <i class="fa fa-times"></i> Delete
                </a>
                @endif
            </td>
        </tr>
        @empty
        <tr>
            <td colspan="3">No Collection Found</td>
        </tr>
        @endforelse
    </tbody>
</table>