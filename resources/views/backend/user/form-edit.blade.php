<div class="col-xs-12">
        <div class="box box-primary">
                <div class="box-body">
                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        {!! Form::label('name') !!}
                        {!! Form::text('name', null, ['class' => 'form-control']) !!}

                        @if($errors->has('name'))
                            <span class="help-block">{{ $errors->first('name') }}</span>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('slug') ? 'has-error' : '' }}">
                        {!! Form::label('slug') !!}
                        {!! Form::text('slug', null, ['class' => 'form-control']) !!}

                        @if($errors->has('slug'))
                            <span class="help-block">{{ $errors->first('slug') }}</span>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                        {!! Form::label('email') !!}
                        {!! Form::text('email', null, ['class' => 'form-control']) !!}
                        @if($errors->has('email'))
                            <span class="help-block">{{ $errors->first('email') }}</span>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('role') ? 'has-error' : '' }}">
                        {!! Form::label('role') !!}
                        @if($user->exists && ($user->id == config('cms.default_user_id') || isset($hideRoleDropdown)))
                        {!! Form::hidden('role', $user->roles->first()->id) !!}
                        <p class="form-control-static">{{ $user->roles->first()->display_name }}</p>
                        @else
                        {!! Form::select('role', App\Models\Config\Role::pluck('display_name', 'id'), $user->exists ? $user->roles->first()->id : null, ['class' => 'form-control', 'placeholder' => '--Pilih level pengguna--'] ) !!}
                        @endif
                        @if ($errors->has('role'))
                            <span class="help-block">{{ $errors->first('role') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        {!! Form::label('position') !!}
                        {!! Form::text('position', null, ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('bio') !!}
                        {!! Form::textarea('bio', null, ['row' => 5, 'class' => 'form-control']) !!}
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                <a href="{{ route('user.index') }}" class="btn btn-default"><i class="fa fa-times"></i> Discard</a>
                <div class="pull-right">
                    <button class="btn btn-primary" type="submit">{{ $user->exists ? 'Update' : 'Save' }}</button>
                </div>
                </div>
                <!-- /.box-footer -->
            </div>
        <!-- /. box -->
</div>