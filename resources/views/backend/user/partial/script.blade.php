@section('script-user')
    <script>
    // Slug feature
    $('#name').on('blur', function(){
        var theName = this.value.toLowerCase().trim(),
            slugInput = $('#slug'),
            theSlug = theName.replace(/&/g, '-dan-')
                            .replace(/[^a-z0-9-]+/g, '-')
                            .replace(/\-\-+/g, '-')
                            .replace(/^-+|-+$/g, '')

            slugInput.val(theSlug);
    });

    // Datetimepicker
    $('#email_verified_at').datetimepicker({
        icons: {
        time: "fa fa-clock-o",
        date: "fa fa-calendar",
        up: "fa fa-arrow-up",
        down: "fa fa-arrow-down",
        clear: "fa fa-trash",
        previous: "fa fa-chevron-left",
        next: "fa fa-chevron-right",
        },
        format: 'YYYY-MM-DD HH:mm:ss',
        showClear: true,
        locale: 'id'
    });
    </script>
@endsection