<table class="table table-bordered">
    <tr>
        <th width="80px">No</th>
        <th>Nama Tag</th>
        <th>Slug</th>
        <th width="100px">Action</th>
    </tr>
    @if($tags->count())
        @foreach($tags as $key => $tag)
    <tr id="tr_{{$tag->id}}">
        <td>{{ ++$key }}</td>
        <td>{{ $tag->name }}</td>
        <td>{{ $tag->slug }}</td>
        <td>
            {!! Form::open(['method' => 'DELETE', 'route' => ['tag.destroy', $tag->id] ]) !!}
            <button onclick="return confirm('Apakah anda yakin. Item ini akan dihapus permanen?')" type="submit" class="btn btn-xs btn-danger">
                <i class="fa fa-times"></i> Delete
            </button>
            {!! Form::close() !!}
        </td>
    </tr>
        @endforeach
    @endif
</table>