<div class='list-group gallery'>
    @forelse ($projects as $project)
    <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
        <a class="thumbnail fancybox" rel="ligthbox" href="{{ $project->image_url }}">
            @if ($project->image_url)
            <img alt="{{ $project->title }}" src="{{ $project->image_thumb_url }}" width="320" height="320" /> 
            @else
            <img alt="{{ $project->title }}" src="http://placehold.it/320x320" />
            @endif
            <div class='text-right'>
                <small class='text-muted'>
                    {{ $project->title }}
                </small>
            </div> <!-- text-right / end -->
        </a>
        <div class="pull-left">
            <a class="btn btn-xs btn-default" href="{{ route('gallery.edit', $project->id) }}" class="text-left"><i class="fa fa-edit"></i></a>
        </div>
        <div class="pull-right">
            {!! Form::open(['method' => 'DELETE', 'route' => ['gallery.destroy', $project->id] ]) !!}
            <button type="submit" onclick="return confirm('Hapus data permanen. anda yakin?')" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>
            {!! Form::close() !!}
        </div>
    </div> <!-- col-6 / end -->
    @empty
     <div style="padding:0 0 0 15px">
        <p>No gallery project available yet.</p> 
     </div>  
    @endforelse
</div> <!-- list-group / end -->
@section('gallery')
<script>
    $(document).ready(function(){
        //FANCYBOX
        //https://github.com/fancyapps/fancyBox
        $(".fancybox").fancybox({
            openEffect: "none",
            closeEffect: "none"
        });
    });
</script> 
@endsection