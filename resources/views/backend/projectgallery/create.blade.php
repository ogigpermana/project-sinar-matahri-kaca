@extends('layouts.backend.app')
@section('title', 'Create new gallery')
@section('content')
<body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
        
          <!--Header section -->
          @include('layouts.backend.navbar')
          <!-- Left side column. contains the logo and sidebar -->
          @include('layouts.backend.sidebar')
        
          <!-- Content Wrapper. Contains page content -->
          <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
              <h1>
                Gallery
                <small>Create new gallery</small>
              </h1>
              <ol class="breadcrumb">
                <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="{{ route('gallery.index') }}">Gallery</a></li>
                <li class="active">Create new gallery</li>
              </ol>
            </section>
        
            <!-- Main content -->
            <section class="content">
                <div class="row">
                <!-- form start -->
                {!! Form::model($gallery, [
                    'method' => 'POST',
                    'route' => 'gallery.store',
                    'files' => TRUE,
                    'id' => 'post-form'
                ]) !!}
                @csrf
                @include('backend.projectgallery.form')
                {!! Form::close() !!}
                </div>
              <!-- ./row -->
            </section>
            <!-- /.content -->
          </div>
          <!-- /.content-wrapper -->
          @include('layouts.backend.foot-note')
        
        </div>
        <!-- ./wrapper -->
@endsection
