<div class="col-xs-12">
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title">Editor Area</h3>
                <div class="pull-right box-tools">
                    <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body pad">

                <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                    {!! Form::label('title') !!}
                    {!! Form::text('title', null, ['class' => 'form-control']) !!}

                    @if ($errors->has('title'))
                        <span class="help-block">{{ $errors->first('title') }}</span>
                    @endif
                </div>

            </div>
            <!-- /.box-body -->
        </div>
    <!-- /.box -->
  </div>
  <div class="col-xs-12">
      <div class="box box-info">
          <div class="box-header">
                <h3 class="box-title"><i class="fa fa-picture-o"></i> Area Gambar</h3>
                <div class="pull-right box-tools">
                    <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
          </div>
          <div class="box-body">
                <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}" style="justify-content:center;text-align:center">
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                            <img src="{{ ($gallery->image_thumb_url) ? $gallery->image_thumb_url : 'https://placehold.it/190x140&text=No+Image' }}" alt="...">
                        </div>
                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                        </div>
                        <div>
                            <span class="btn btn-default btn-file"><span class="fileinput-new">Pilih Gambar</span><span class="fileinput-exists">Ganti</span>{!! Form::file('image') !!}</span>
                                <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Hapus</a>
                        </div>
                    </div>    
        
                        @if($errors->has('image'))
                            <span class="help-block">{{ $errors->first('image') }}</span>
                        @endif
                </div>
          </div>
          <div class="box-footer">
            {{Form::button('<i class="fa fa-floppy-o"></i> Simpan', array('type' => 'submit', 'class' => 'btn btn-info pull-right'))}}
          </div>
        <!-- /.box-footer -->
      </div>
  </div>