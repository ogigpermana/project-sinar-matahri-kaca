<table class="table table-bordered">
    <tr>
        <th width="80px">No</th>
        <th>Nama</th>
        <th>Email</th>
        <th>Nama Domain</th>
        <th>Isi komentar</th>
        <th>ID post</th>
        <th width="100px">Action</th>
    </tr>
    @if($comments->count())
        @foreach($comments as $key => $comment)
    <tr id="tr_{{$comment->id}}">
        <td>{{ ++$key }}</td>
        <td>{{ $comment->user_name }}</td>
        <td>{{ $comment->user_email }}</td>
        <td>{{ $comment->user_site_url }}</td>
        <td>{{ $comment->body }}</td>
        <td>{{ $comment->post_id }}</td>
        <td>
            {!! Form::open(['method' => 'DELETE', 'route' => ['comment.destroy', $comment->id] ]) !!}
            <button onclick="return confirm('Apakah anda yakin. Item ini akan dihapus permanen?')" type="submit" class="btn btn-xs btn-danger">
                <i class="fa fa-times"></i> Delete
            </button>
            {!! Form::close() !!}
        </td>
    </tr>
        @endforeach
    @endif
</table>