@extends('layouts.backend.app')
@section('title', 'List Komentar')
@section('content')
<body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
        
          <!--Header section -->
          @include('layouts.backend.navbar')
          <!-- Left side column. contains the logo and sidebar -->
          @include('layouts.backend.sidebar')
        
          <!-- Content Wrapper. Contains page content -->
          <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
              <h1>
                Komentar
                <small>Menampilkan semua komentar</small>
              </h1>
              <ol class="breadcrumb">
                <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="{{ route('comment.index') }}">Komentar</a></li>
                <li class="active">Semua komentar</li>
              </ol>
            </section>
        
            <!-- Main content -->
            <section class="content">
                <div class="row">
                  <div class="col-xs-12">
                      @if(session('message'))
                      <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        {{ session('message') }}
                        </div>
                      @endif
                        <div class="box box-info">
                            <div class="box-header with-border">
                              <div class="pull-left">
                                  <h3 class="heeader">Komentar</h3>
                              </div>
                              <div class="pull-right box-tools">
                                  <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                                      <i class="fa fa-minus"></i></button>
                                  <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
                                      <i class="fa fa-times"></i></button>
                              </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body table-responsive no-padding">
                                  @include('backend.comment.table')
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer clearfix">
                                <nav class="pull-left">
                                    <small>{{ $commentsCount }} {{ str_plural('Item', $commentsCount) }}</small>
                                </nav>
                                <nav class="pagination pagination-sm no-margin pull-right">
                                        {{ $comments->render() }}
                                </nav>
                            </div>
                        </div>
                    <!-- /.box -->
                  </div>
                </div>
              <!-- ./row -->
            </section>
            <!-- /.content -->
          </div>
          <!-- /.content-wrapper -->
          @include('layouts.backend.foot-note')
        
        </div>
        <!-- ./wrapper -->
@endsection
@section('script-pagination')
    <script>
        $('ul.pagination').addClass('no-margin pagination-sm');
    </script>
@endsection
