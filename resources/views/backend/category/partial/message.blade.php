@if (session('message'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    {{ session('message') }}
    </div>

@elseif(session('trash-message'))
<div class="alert alert-success alert-dismissible" role="alert" style="margin:5px">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?php list($message, $postId) = session('trash-message') ?>
        {!! Form::open(['method' => 'PUT', 'route' => ['blog.restore', $postId]]) !!}
        <strong>Success!</strong> {{ $message }}
        @csrf
        <button class="btn btn-xs btn-warning" type="submit"><i class="fa fa-undo"></i> Undo</button>
        {!! Form::close() !!}
    </div>
@endif