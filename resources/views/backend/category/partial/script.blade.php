@section('script-category')
    <script>
    // Slug feature
    $('#title').on('blur', function(){
        var theTitle = this.value.toLowerCase().trim(),
            slugInput = $('#slug'),
            theSlug = theTitle.replace(/&/g, '-dan-')
                            .replace(/[^a-z0-9-]+/g, '-')
                            .replace(/\-\-+/g, '-')
                            .replace(/^-+|-+$/g, '')

            slugInput.val(theSlug);
    });
    </script>
@endsection