<div class="col-xs-12">
        <div class="box box-primary">
                <div class="box-body">
                    <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                        {!! Form::label('title') !!}
                        {!! Form::text('title', null, ['class' => 'form-control']) !!}

                        @if($errors->has('title'))
                            <span class="help-block">{{ $errors->first('title') }}</span>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('slug') ? 'has-error' : '' }}">
                        {!! Form::label('slug') !!}
                        {!! Form::text('slug', null, ['class' => 'form-control', 'readonly' => 'readonly']) !!}
                        @if($errors->has('slug'))
                            <span class="help-block">{{ $errors->first('slug') }}</span>
                        @endif
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                <div class="pull-right">
                    <button class="btn btn-primary" type="submit">{{ $category->exists ? 'Update' : 'Save' }}</button>
                </div>
                <a href="{{ route('category.index') }}" class="btn btn-default"><i class="fa fa-times"></i> Discard</a>
                </div>
                <!-- /.box-footer -->
            </div>
        <!-- /. box -->
</div>