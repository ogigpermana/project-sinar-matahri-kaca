<table class="table table-hover table-bordered">
    <thead>
        <tr class="info">
            <th width="">Judul Kategori</th>
            <th width="150">Jumlah pos</th>
            <th width="140">Action</th>
        </tr>
    </thead>
    <tbody>
        @forelse($categories as $category)
        <tr>
            <td>{{  $category->title }}</td>
            <td>{{ $category->posts->count() }}</td>
            <td>
                {!! Form::open(['method' => 'DELETE', 'route' => ['category.destroy', $category->id] ]) !!}
                <a class="btn btn-xs btn-primary" href="{{ route('category.edit', $category->id) }}"><i class="fa fa-pencil"></i> Edit</a>
                @if ($category->id == config('cms.default_category_id'))
                <button onclick="return false" type="submit" class="btn btn-xs btn-danger" disabled>
                    <i class="fa fa-times"></i> Delete
                </button>
                @else
                <button onclick="return confirm('Apakah anda yakin. Item ini akan dihapus permanen?')" type="submit" class="btn btn-xs btn-danger">
                    <i class="fa fa-times"></i> Delete
                </button>
                @endif
                {!! Form::close() !!}
            </td>
        </tr>
        @empty
        <tr>
            <td colspan="3">No Collection Found</td>
        </tr>
        @endforelse
    </tbody>
</table>