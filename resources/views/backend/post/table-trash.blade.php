<table class="table table-bordered table-striped table-hover">
    <tr>
        <th>Judul</th>
        <th width="175">Pemilik</th>
        <th width="175">Kategori</th>
        <th width="175">Tanggal buat</th>
        <th width="75">Action</th>
    </tr>
    @forelse ($posts as $post)
    <tr>
        <td>{{ $post->title }}</td>
        <td>
        {{ $post->user->name }}
        </td>
        <td>{{ $post->category->title }}</td>
        <td>
            <abbr title="{{ $post->dateFormatted(true) }}">{{ $post->dateFormatted() }}</abbr>
        </td>
        <td>
            {!! Form::open(['style' => 'display:inline-block;', 'method' => 'PUT', 'route' => ['blog.restore', $post->id] ]) !!}
            @if(check_user_permissions(request(), "Post@restore", $post->id))
            <button title="Restore" type="submit" class="btn btn-xs btn-default"><i class="fa fa-refresh"></i></button>
            @else
            <button title="Restore" type="button" onclick="return false;" class="btn btn-xs btn-default" disabled><i class="fa fa-refresh"></i></button>
            @endif
            {!! Form::close() !!}

            {!! Form::open(['style' => 'display:inline-block;', 'method' => 'DELETE', 'route' => ['blog.force-destroy', $post->id] ]) !!}
            @if(check_user_permissions(request(), "Post@forceDestroy", $post->id))
            <button title="Hapus permanent" onclick="return confirm('Artikel ini akan dihapus secara permanen. Apakah anda yakin?')" type="submit" class="btn btn-xs btn-rounded btn-danger"><i class="fa fa-times"></i></button>
            @else
            <button title="Hapus permanent" onclick="return false;" type="submit" class="btn btn-xs btn-rounded btn-danger" disabled><i class="fa fa-times"></i></button>
            @endif
            {!! Form::close() !!}
        </td>
    </tr>
    @empty
    <td colspan="6">No Collection Found</td>
    @endforelse
</table>