<div class="col-xs-9">
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title">Editor Area</h3>
                <div class="pull-right box-tools">
                    <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body pad">

                <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                    {!! Form::label('title') !!}
                    {!! Form::text('title', null, ['class' => 'form-control']) !!}

                    @if ($errors->has('title'))
                        <span class="help-block">{{ $errors->first('title') }}</span>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('slug') ? 'has-error' : '' }}">
                    {!! Form::label('slug') !!}
                    {!! Form::text('slug', null, ['class' => 'form-control', 'readonly' => 'readonly']) !!}

                    @if ($errors->has('slug'))
                        <span class="help-block">{{ $errors->first('slug') }}</span>
                    @endif
                </div>

                <div class="form-group">
                    {!! Form::label('ringkasan artikel') !!}
                    {!! Form::textarea('excerpt', null, ['class' => 'form-control my-editor']) !!}
                </div>

                <div class="form-group {{ $errors->has('body') ? 'has-error' : '' }}">
                    {!! Form::label('konten artikel') !!}
                    {!! Form::textarea('body', null, ['class' => 'form-control my-editor']) !!}

                    @if ($errors->has('body'))
                        <span class="help-block">{{ $errors->first('body') }}</span>
                    @endif
                </div>

            </div>
            <!-- /.box-body -->
        </div>
    <!-- /.box -->
  </div>
  <div class="col-xs-3">
    <div class="box box-info">
        <div class="box-header">
            <h3 class="box-title">Tanggal & Kategori</h3>
            <div class="pull-right box-tools">
                <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                    <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
                    <i class="fa fa-times"></i></button>
            </div>
        </div>
        <div class="box-body pad">
            <div class="form-group {{ $errors->has('published_at') ? 'has-error' : '' }}">
                {!! Form::label('published_at', 'Tanggal Terbit') !!}
                <div class='input-group date' id='datetimepicker2'>
                    {!! Form::text('published_at', null, ['class' => 'form-control', 'autofocus' => 'autofocus']) !!}
                    <span class="input-group-addon" id="datetimepicker">
                        <span class="fa fa-calendar"></span>
                    </span>
                </div>
                @if ($errors->has('published_at'))
                    <span class="help-block">{{ $errors->first('published_at') }}</span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('category_id') ? 'has-error' : '' }}">
                {!! Form::label('category_id', 'Kategori') !!}
                {!! Form::select('category_id', \App\Models\Site\Category::pluck('title', 'id') ,null, ['class' => 'form-control', 'placeholder' => '--Pilih Kategori--']) !!}

                @if ($errors->has('category_id'))
                    <span class="help-block">{{ $errors->first('category_id') }}</span>
                @endif
            </div>
        </div>
    </div>
  </div>
  <div class="col-xs-3">
      <div class="box box-info">
          <div class="box-header">
              <h3 class="box-title">Tags</h3>
          </div>
          <div class="box-body">
            <div class="form-group">                
                {!! Form::text('post_tags', null, ['class' => 'form-control']) !!}              
            </div>
          </div>
      </div>
  </div>
  <div class="col-xs-3">
      <div class="box box-info">
          <div class="box-header">
                <h3 class="box-title"><i class="fa fa-picture-o"></i> Area Gambar</h3>
                <div class="pull-right box-tools">
                    <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
          </div>
          <div class="box-body">
                <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}" style="justify-content:center;text-align:center">
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                            <img src="{{ ($post->image_thumb_url) ? $post->image_thumb_url : 'https://placehold.it/190x140&text=No+Image' }}" alt="...">
                        </div>
                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                        </div>
                        <div>
                            <span class="btn btn-default btn-file"><span class="fileinput-new">Pilih Gambar</span><span class="fileinput-exists">Ganti</span>{!! Form::file('image') !!}</span>
                                <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Hapus</a>
                        </div>
                    </div>    
        
                        @if($errors->has('image'))
                            <span class="help-block">{{ $errors->first('image') }}</span>
                        @endif
                </div>
          </div>
          <div class="box-footer">
            <a id="draft-btn" class="btn btn-default"><i class="fa fa-pencil"></i> Draft</a>
            {{Form::button('<i class="fa fa-floppy-o"></i> Simpan', array('type' => 'submit', 'class' => 'btn btn-info pull-right'))}}
          </div>
        <!-- /.box-footer -->
      </div>
  </div>