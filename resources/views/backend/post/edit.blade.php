@extends('layouts.backend.app')
@section('title', 'Create new post')
@section('content')
<body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
        
          <!--Header section -->
          @include('layouts.backend.navbar')
          <!-- Left side column. contains the logo and sidebar -->
          @include('layouts.backend.sidebar')
        
          <!-- Content Wrapper. Contains page content -->
          <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
              <h1>
                Blog
                <small>Edit post</small>
              </h1>
              <ol class="breadcrumb">
                <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="{{ route('blog.index') }}">Blog</a></li>
                <li class="active">Edit post</li>
              </ol>
            </section>
        
            <!-- Main content -->
            <section class="content">
                <div class="row">
                <!-- form start -->
                {!! Form::model($post, [
                    'method' => 'PUT',
                    'route' => ['blog.update', $post->id],
                    'files' => TRUE,
                    'id' => 'post-form'
                    ]) !!}
                @csrf
                @include('backend.post.form')
                {!! Form::close() !!}
                </div>
              <!-- ./row -->
            </section>
            <!-- /.content -->
          </div>
          <!-- /.content-wrapper -->
          @include('layouts.backend.foot-note')
        
        </div>
        <!-- ./wrapper -->
        @include('backend.post.partial.script')
@endsection
