<table class="table table-bordered table-striped table-hover">
    <tr class="info">
        <th>Judul</th>
        <th width="175">Pemilik</th>
        <th width="175">Kategori</th>
        <th width="175">Tanggal buat</th>
        <th width="75">Action</th>
    </tr>
    @forelse ($posts as $post)
    <tr>
        <td>{{ $post->title }}</td>
        <td>
        {{ $post->user->name }}
        </td>
        <td>{{ $post->category->title }}</td>
        <td>
            <abbr title="{{ $post->dateFormatted(true) }}">{{ $post->dateFormatted() }}</abbr> | {!! $post->publicationLabel() !!}
        </td>
        <td>
            {!! Form::open(['method' => 'DELETE', 'route' => ['blog.destroy', $post->id] ]) !!}
            @if(check_user_permissions(request(), "Post@edit", $post->id))
            <a class="btn btn-xs btn-default" href="{{ route('blog.edit', $post->id) }}"><i class="fa fa-edit"></i></a>
            @else
            <a class="btn btn-xs btn-default" href="#" aria-disabled="true"><i class="fa fa-edit"></i></a>
            @endif
            @if(check_user_permissions(request(), "Post@destroy", $post->id))
            <button type="submit" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>
            @else
            <button type="button" onclick="return false;" class="btn btn-xs btn-danger" disabled><i class="fa fa-trash"></i></button>
            @endif
            {!! Form::close() !!}
        </td>
    </tr>
    @empty
    <td colspan="6">No Collection Found</td>
    @endforelse
</table>