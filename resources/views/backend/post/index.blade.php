@extends('layouts.backend.app')
@section('title', 'List Posts')
@section('content')
<body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
        
          <!--Header section -->
          @include('layouts.backend.navbar')
          <!-- Left side column. contains the logo and sidebar -->
          @include('layouts.backend.sidebar')
        
          <!-- Content Wrapper. Contains page content -->
          <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
              <h1>
                Blog
                <small>Display all blog posts</small>
              </h1>
              <ol class="breadcrumb">
                <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="{{ route('blog.index') }}">Blog</a></li>
                <li class="active">All posts</li>
              </ol>
            </section>
        
            <!-- Main content -->
            <section class="content">
                <div class="row">
                  <div class="col-xs-12">
                      @include('backend.post.partial.message')
                        <div class="box box-info">
                            <div class="box-header with-border">
                              <div class="pull-left">
                                <a href="{{ route('blog.create') }}" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Buat artikel baru</a>
                              </div>
                              <div class="pull-right box-tools">
                                  <?php $links = [] ?>
                                  @foreach($statusList as $key => $value)
                                      @if($value)
                                      <?php $selected = Request::get('status') == $key ? 'selected-status' : '' ?>
                                      <?php $links[] = "<a class=\"{$selected}\" href=\"?status={$key}\">" . ucwords($key) . "({$value})</a> " ?>
                                      @endif
                                  @endforeach 
                                  {!!  implode(' | ', $links) !!}
                                  <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                                      <i class="fa fa-minus"></i></button>
                                  <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
                                      <i class="fa fa-times"></i></button>
                              </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body table-responsive no-padding">
                              @if ($onlyTrashed)
                                  @include('backend.post.table-trash')
                              @else
                                  @include('backend.post.table')
                              @endif
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer clearfix">
                                <nav class="pull-left">
                                    <small>{{ $postCount }} {{ str_plural('Item', $postCount) }}</small>
                                </nav>
                                <nav class="pagination pagination-sm no-margin pull-right">
                                        {{ $posts->appends(Request::query())->render() }}
                                </nav>
                            </div>
                        </div>
                    <!-- /.box -->
                  </div>
                </div>
              <!-- ./row -->
            </section>
            <!-- /.content -->
          </div>
          <!-- /.content-wrapper -->
          @include('layouts.backend.foot-note')
        
        </div>
        <!-- ./wrapper -->
@endsection
@section('script-pagination')
    <script>
        $('ul.pagination').addClass('no-margin pagination-sm');
    </script>
@endsection
