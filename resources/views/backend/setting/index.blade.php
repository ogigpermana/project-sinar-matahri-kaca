@extends('layouts.backend.app')
@section('title', 'Setting application page')
@section('content')
<body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
        
          <!--Header section -->
          @include('layouts.backend.navbar')
          <!-- Left side column. contains the logo and sidebar -->
          @include('layouts.backend.sidebar')
        
          <!-- Content Wrapper. Contains page content -->
          <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
              <h1>
                Dasbhboard
              </h1>
              <ol class="breadcrumb">
                <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active"><i class="fa fa-cogs"></i> App Setting</li>
              </ol>
            </section>
        
            <!-- Main content -->
            <section class="content">
                <div class="row">
                  <div class="col-xs-12">
                    
                        @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
    
                    <form method="post" action="{{ route('settings.store') }}" class="form-horizontal" role="form">
                        @csrf
    
                        @if(count(config('setting_fields', [])) )
    
                            @foreach(config('setting_fields') as $section => $fields)
                                <div class="box box-info">
                                    <div class="box-header">
                                        <i class="{{ array_get($fields, 'icon', 'fa fa-flash') }}"></i>
                                        {{ $fields['title'] }}
                                        <div class="pull-right box-tools">
                                            <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                                                <i class="fa fa-minus"></i></button>
                                            <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
                                                <i class="fa fa-times"></i></button>
                                        </div>
                                    </div>
    
                                    <div class="box-body">
                                        <p class="text-muted">{{ $fields['desc'] }}</p>
                                    </div>
    
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-md-7  col-md-offset-2">
                                                @foreach($fields['elements'] as $field)
                                                    @includeIf('backend.setting.fields.' . $field['type'] )
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
    
                                </div>
                                <!-- end panel for {{ $fields['title'] }} -->
                            @endforeach
    
                        @endif
    
                        <div class="row m-b-md">
                            <div class="col-md-12">
                                <button class="btn-primary btn">
                                    Save Settings
                                </button>
                            </div>
                        </div>
                    </form>
                      <!-- /.box-body -->
                    <!-- /.box -->
                  </div>
                </div>
              <!-- ./row -->
            </section>
            <!-- /.content -->
          </div>
          <!-- /.content-wrapper -->
          @include('layouts.backend.foot-note')
        
        </div>
        <!-- ./wrapper -->
@endsection
