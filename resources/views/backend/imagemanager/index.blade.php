@extends('layouts.backend.app')
@section('title', 'Image Manager')
@section('content')
<body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
        
          <!--Header section -->
          @include('layouts.backend.navbar')
          <!-- Left side column. contains the logo and sidebar -->
          @include('layouts.backend.sidebar')
        
          <!-- Content Wrapper. Contains page content -->
          <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
              <h1>
                Blog
                <small>Upload new image</small>
              </h1>
              <ol class="breadcrumb">
                <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="{{ route('blog.index') }}">Blog</a></li>
                <li class="active">Image Manager</li>
              </ol>
            </section>
        
            <!-- Main content -->
            <section class="content">
                <div class="row">
                  <div class="col-xs-12">
                      <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">Image Manager</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                    </button>
                                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="input-group">
                                    <span class="input-group-btn">
                                        <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                                        <i class="fa fa-picture-o"></i> Choose
                                        </a>
                                    </span>
                                    <input id="thumbnail" class="form-control" type="text" name="filepath">
                                </div>
                                <img id="holder" style="margin-top:15px;max-height:100px;">
                            </div>
                      </div>
                    <!-- /.box -->
                  </div>
                </div>
              <!-- ./row -->
            </section>
            <!-- /.content -->
          </div>
          <!-- /.content-wrapper -->
          @include('layouts.backend.foot-note')
        
        </div>
        <!-- ./wrapper -->
@endsection
@section('script-lfm')
    <script>
      // Standalone image manager
      $('#lfm').filemanager('image');
    </script>
@endsection
