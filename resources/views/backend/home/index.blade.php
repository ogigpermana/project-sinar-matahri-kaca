@extends('layouts.backend.app')
@section('title', 'Welcome to admin panel')
@section('content')
<body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
        
          <!--Header section -->
          @include('layouts.backend.navbar')
          <!-- Left side column. contains the logo and sidebar -->
          @include('layouts.backend.sidebar')
        
          <!-- Content Wrapper. Contains page content -->
          <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
              <h1>
                Dasbhboard
              </h1>
              <ol class="breadcrumb">
                <li class="active"><i class="fa fa-dashboard"></i> Dashboard</li>
              </ol>
            </section>
        
            <!-- Main content -->
            <section class="content">
                <div class="row">
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-aqua"><i class="fa fa-newspaper-o"></i></span>
          
                      <div class="info-box-content">
                        <span class="info-box-text">{{ str_plural('post', $postsCount) }}</span>
                        <span class="info-box-number">{{ $postsCount }}</span>
                      </div>
                      <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                  </div>
                  <!-- /.col -->
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-red"><i class="fa fa-comment"></i></span>
          
                      <div class="info-box-content">
                        <span class="info-box-text">{{ str_plural('comment', $commentsCount) }}</span>
                        <span class="info-box-number">{{ $commentsCount }}</span>
                      </div>
                      <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                  </div>
                  <!-- /.col -->
          
                  <!-- fix for small devices only -->
                  <div class="clearfix visible-sm-block"></div>
          
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-green"><i class="fa fa-globe"></i></span>
          
                      <div class="info-box-content">
                        <span class="info-box-text">{{ str_plural('Visitor', $visitorCount) }}</span>
                        <span class="info-box-number">{{ $visitorCount }}</span>
                      </div>
                      <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                  </div>
                  <!-- /.col -->
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>
          
                      <div class="info-box-content">
                        <span class="info-box-text">{{ str_plural('member', $usersCount) }}</span>
                        <span class="info-box-number">{{ $usersCount }}</span>
                      </div>
                      <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                  </div>
                  <!-- /.col -->
                  <div class="col-xs-12">
                    <div class="box box-info">
                      <div class="box-header">
                          <div class="pull-right box-tools">
                              <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                                  <i class="fa fa-minus"></i></button>
                              <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
                                  <i class="fa fa-times"></i></button>
                          </div>
                      </div>
                      <!-- /.box-header -->
                      <div class="box-body ">
                            <h3><i class="fa fa-flag"></i> Welcome to {{ \setting('app_name') }}!</h3>
                            <p class="lead text-muted">Hallo {{Auth::user()->name}}, Welcome to {{ \setting('app_name') }}</p>
        
                            <h4>Get started</h4>
                            <p><a href="{{ route('blog.create') }}" class="btn btn-primary">Tulis artikel <i class="fa fa-pencil"></i> </a> </p>
                      </div>
                      <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                  </div>
                  {{-- <div class="col-xs-12">
                      <div class="box box-info">
                          <div class="box-header with-border">
                            <h3 class="box-title"><i class="fa fa-newspaper-o"></i> Artikel produk terbaru</h3>
                            <div class="pull-right box-tools">
                                <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                                    <i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
                                    <i class="fa fa-times"></i></button>
                            </div>
                          </div>
                          <!-- /.box-header -->
                          <div class="box-body">
                            <ul class="products-list product-list-in-box">
                              @forelse ($posts as $post)
                              <li class="item">
                                  <div class="product-img">
                                    @if ($post->image_url)
                                    <img src="{{ $post->image_url }}" alt="{{ $post->title }}">
                                    @else
                                    <img src="/frontend/images/about.jpg" alt="{{ $post->title }}">
                                    @endif
                                  </div>
                                  <div class="product-info">
                                    <a href="javascript:void(0)" class="product-title">{{ $post->title }}
                                      <span class="label label-default pull-right">{{ $post->category->title }}</span></a>
                                    <span class="product-description">
                                          {!! str_limit($post->excerpt, 100) !!}
                                    </span>
                                  </div>
                                </li>
                                <!-- /.item -->
                              @empty
                                  Tidak ada daftar produk yang ditemukan.
                              @endforelse
                            </ul>
                          </div>
                          <!-- /.box-body -->
                        </div>
                  </div> --}}
                  @role('admin')
                  <div class="col-xs-12">
                    <div class="box box-info">
                      <!-- /.box-header -->
                          <div class="box-header with-border">
                            <h3 class="box-title">
                                <i class="fa fa-wrench"></i> Pengaturan Website
                            </h3>
                            <div class="pull-right box-tools">
                                <a href="{{ route('settings') }}" class="btn btn-info btn-sm" data-toggle="tooltip" title="" data-original-title="Edit">
                                    <i class="fa fa-edit"></i></a>
                                <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                                    <i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
                                    <i class="fa fa-times"></i></button>
                            </div>
                          </div>
                          <div class="box-body table-responsive no-padding">
                              <table class="table table-bordered table-striped table-hover">
                                  <tr class="info">
                                      <th>Pengaturan</th>
                                      <th>Aksesor</th>
                                      <th>Nilai</th>
                                  </tr>
                                  @forelse($settings as $setting)
                                  <tr>
                                      <td>{{ $setting->name }}</td>
                                      <td><span class="label label-info">\setting('{{ $setting->name }}')</span></td>
                                      <td>{{ $setting->val }}</td>
                                  </tr>
                                      @empty
                                  <tr>
                                      <td colspan="3" class="text-center">Tidak ada pengaturan aplikasi yang tersimpan.</td>
                                  </tr>
                                  @endforelse
                              </table>
                          </div>
                      <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                  </div>
                  @endrole
                </div>
              <!-- ./row -->
            </section>
            <!-- /.content -->
          </div>
          <!-- /.content-wrapper -->
          @include('layouts.backend.foot-note')
        
        </div>
        <!-- ./wrapper -->
@endsection
