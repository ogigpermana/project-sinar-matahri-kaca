@extends('layouts.backend.app')
@section('title', 'Change password')
@section('content')
<body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
          <!--Header section -->
          @include('layouts.backend.navbar')
          <!-- Left side column. contains the logo and sidebar -->
          @include('layouts.backend.sidebar')
          <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Ubah
            <small>Password</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Ubah Password</li>
          </ol>
        </section>
        
        <!-- Main content -->
        <section class="content">
          <div class="row">
              <div class="col-md-12">
                  <!-- Widget: user widget style 1 -->
                  <div class="box box-widget widget-user">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-aqua-active">
                      <h3 class="widget-user-username">{{ Auth::user()->name }}</h3>
                      <h5 class="widget-user-desc">{{ Auth::user()->position }}</h5>
                    </div>
                    <div class="widget-user-image">
                      <img class="img-circle" src="{{ Auth::user()->avatar() }}" alt="User Avatar">
                    </div>
                    <div class="box-footer">
                      <div class="row">
                        <!-- /.col -->
                        <div class="col-sm-12">
                          <div class="description-block">
                            <h5 class="description-header">{{ Auth::user()->posts->count() }}</h5>
                            <span class="description-text">{{ str_plural('POST', Auth::user()->posts->count()) }}</span>
                          </div>
                          <!-- /.description-block -->
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->
                    </div>
                  </div>
                  <!-- /.widget-user -->
              </div>
              <div class="col-md-12">
                  <!-- general form elements -->
                  <div class="box box-info">
                      <div class="box-header with-border">
                        <h3 class="box-title">Ubah Password</h3>
                      </div>
                      <form class="form-horizontal" method="POST" action="{{ route('change.password') }}">
                          {{ csrf_field() }}
                          <div class="box-body">
                              @include('backend.partial.message')
                              <div class="form-group{{ $errors->has('current-password') ? ' has-error' : '' }}">
                                  <label for="current-password" class="col-sm-2 control-label">Password Lama</label>
       
                                  <div class="col-sm-10">
                                      <input id="current-password" type="password" class="form-control" name="current-password">
       
                                      @if ($errors->has('current-password'))
                                          <span class="help-block">
                                              <strong>{{ $errors->first('current-password') }}</strong>
                                          </span>
                                      @endif
                                  </div>
                              </div>
       
                              <div class="form-group{{ $errors->has('new-password') ? ' has-error' : '' }}">
                                  <label for="new-password" class="col-sm-2 control-label">Password Baru</label>
       
                                  <div class="col-sm-10">
                                      <input id="new-password" type="password" class="form-control" name="new-password">
       
                                      @if ($errors->has('new-password'))
                                          <span class="help-block">
                                              <strong>{{ $errors->first('new-password') }}</strong>
                                          </span>
                                      @endif
                                  </div>
                              </div>
       
                              <div class="form-group">
                                  <label for="new-password-confirm" class="col-sm-2 control-label">Konfirmasi Password</label>
       
                                  <div class="col-sm-10">
                                      <input id="new-password-confirm" type="password" class="form-control" name="new-password_confirmation">
                                  </div>
                              </div>
                          </div>
                          <div class="box-footer">
                              <a href="{{ route('home') }}" class="btn btn-default">Cancel</a>
                              <button type="submit" class="btn btn-info pull-right"><i class="fa fa-refresh"></i> Ubah Password</button>
                          </div>
                      </form>
                  </div>
                  <!-- /.box -->
        
                </div>
          </div>
        </section>
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->
        </div>
@endsection