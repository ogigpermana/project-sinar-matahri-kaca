@extends('layouts.backend.app')
@section('title', 'Edit profile')
@section('content')
<body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
        
          <!--Header section -->
          @include('layouts.backend.navbar')
          <!-- Left side column. contains the logo and sidebar -->
          @include('layouts.backend.sidebar')
        
          <!-- Content Wrapper. Contains page content -->
          <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
              <h1>
                Profile
                <small>Edit profile</small>
              </h1>
              <ol class="breadcrumb">
                <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active">Edit profile</li>
              </ol>
            </section>
        
            <!-- Main content -->
            <section class="content">
                @if (session('message'))
                <div class="alert alert-success">
                    <p>{{ session('message') }}</p>
                </div>
                @endif
                <div class="row">
                <!-- form start -->
                {!! Form::model($user, [
                  'method' => 'PUT',
                  'url' => "/home/edit-profile",
                  'id' => 'user-form'
                  ]) !!}

                  @csrf
                  @include('backend.user.form-edit', ['hideRoleDropdown' => true])
                
                {!! Form::close() !!}
                </div>
              <!-- ./row -->
            </section>
            <!-- /.content -->
          </div>
          <!-- /.content-wrapper -->
          @include('layouts.backend.foot-note')
        
        </div>
        <!-- ./wrapper -->
        @include('backend.user.partial.script')
@endsection
