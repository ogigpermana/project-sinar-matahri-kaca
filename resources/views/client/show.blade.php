@extends('layouts.front')
@section('title')
    {{ $post->title }}
@endsection
@section('content')
    
<section id="blog">
        <div class="blog container">
            <div class="row" style="margin-top:100px;">
                <div class="col-md-8">

                    <div class="blog-item">
                        @if ($post->image_url)    
                        <img class="img-responsive img-blog" src="{{ $post->image_url }}" width="100%" alt="" />
                        @else
                        <img class="img-responsive img-blog" src="/frontend/images/about.jpg" width="100%" alt="" />   
                        @endif
                        <div class="blog-content">
                            <h2>{{ $post->title }}</h2>
                            @php
                                $commentsNumber = $post->comments->count()
                            @endphp
                            <div class="post-meta">
                                <i class="fa fa-user"></i> <a href="{{ route('user', $post->user->slug) }}">{{ $post->user->name }}</a> <i class="fa fa-angle-double-right"></i>
                                <i class="fa fa-calendar"></i> <a href="#">{{ $post->date }}</a> <i class="fa fa-angle-double-right"></i>
                                <i class="fa fa-folder"></i> <a href="{{ route('category', $post->category->slug) }}" class="blog_cat">{{ $post->category->title }}</a><i class="fa fa-angle-double-right"></i>
                                <i class="fa fa-comment"></i> <a href="#post-comments">{{ $commentsNumber }} {{ str_plural('Comment', $commentsNumber) }}</a>
                            </div>
                            <p>{!! $post->body !!}</p>
                            <div class="inner-meta">
                                <ul class="tags">
                                    @foreach ($post->tags as $tag)    
                                        <li><a href="{{ route('tag', $tag->slug) }}">{{ $tag->name }}</a></li>
                                    @endforeach
                                </ul>
                                <div style="margin-top:25px; margin-bottom:25px;">
                                    <a href="https://bit.ly/2O9mrkV" class="button red">
                                        <strong class="title">Pesan Langsung Disini <i class="far fa-hand-point-right"></i></strong>
                                        <span class="details">
                                            Chat via whatsapp untuk info lebih jelas.
                                        </span>
                                        <span class="price">
                                            <i class="fab fa-whatsapp" style="color:lightgreen;"></i> <strong>&rsaquo;</strong>
                                        </span>
                                    </a>
                                </div>
                            </div>
                            <!--/.blog-item-->
                            <div class="blog-item" style="margin-top:50px; margin-bottom:50px;">
                                <div class="row">
                                    <div class="col-md-2">
                                        <img src="{{ $post->user->avatar() }}" alt="{{ $post->user->name }}" style="width:120px; height:120px;border-radius:50%; padding:10px">
                                    </div>
                                    <div class="col-md-10" style="padding-left:30px;">
                                        <p style="padding: 10px 10px 0 0;">
                                            <span><strong><a href="{{ route('user', $post->user->slug) }}">{{ $post->user->name }}</a></strong></span> <br>
                                            @php
                                                $postCount = $post->user->posts()->published()->count();
                                            @endphp
                                            <i class="fa fa-clone"></i> {{ $postCount }} {{ str_plural('post', $postCount) }} <br>
                                            <span>{!! $post->user->bio_html !!}</span> 
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <hr style="background-color: #fff;
                            border-top: 2px dashed #8c8b8b;">
                            <!-- Comment Section here -->
                            @include('client.widgets.comment')

                        </div>
                    </div>
                    
                </div>
                <!--/.col-md-8-->
                <!--Sidebar -->
                @include('client.widgets.sidebar')
            </div>
            <!--/.row-->
        </div>
    </section>
    <!--/#blog-->
@endsection