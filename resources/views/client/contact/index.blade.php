@extends('layouts.front')
@section('title', 'Contact Us')
@section('content')
<div class="container" style="margin-top:100px; margin-bottom:100px">
	<h1 class="mb-2 text-center">Hubungi Kami</h1>
	<p class="text-center">Kontak kami melalui form dibawah ini untuk pemesanan melalui email langsung.</p>
	
	@if(session('message'))
	<div class='alert alert-success'>
		{{ session('message') }}
	</div>
	@endif
	
	<div class="col-md-offset-2  col-md-8">
		<form class="form-horizontal" method="POST" action="/contact">
			@csrf
		<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
			<label for="Name">Name: </label>
			<input type="text" class="form-control" id="name" placeholder="Your name" name="name" required>
			@if ($errors->has('name'))
				<span class="help-block">
					<strong>{{ $errors->first('name') }}</strong>
				</span>
			@endif
		</div>

		<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
			<label for="email">Email: </label>
			<input type="text" class="form-control" id="email" placeholder="john@example.com" name="email" required>
			@if ($errors->has('email'))
				<span class="help-block">
					<strong>{{ $errors->first('email') }}</strong>
				</span>
			@endif
		</div>

		<div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
			<label for="message">message: </label>
			<textarea type="text" rows="6" class="form-control luna-message" id="message" placeholder="Type your messages here" name="message" required></textarea>
			@if ($errors->has('message'))
				<span class="help-block">
					<strong>{{ $errors->first('message') }}</strong>
				</span>
			@endif
		</div>

		<div class="form-group{{ $errors->has('CaptchaCode') ? ' has-error' : '' }}">
			<label for="captcha">Captcha:</label>
			<p><small style="color:#FF5126">Silahkan isi kode captcha berikut ini untuk memastikan bahwa anda bukan robot.</small></p>
				{!! captcha_image_html('ContactCaptcha') !!}
				<input class="form-control" type="text" id="CaptchaCode" name="CaptchaCode" style="margin-top:5px;">
				@if ($errors->has('CaptchaCode'))
					<span class="help-block">
						<strong>{{ $errors->first('CaptchaCode') }}</strong>
					</span>
				@endif
		</div>

		<div class="form-group">
			<button type="submit" class="btn btn-primary" value="Send">Send</button>
		</div>
		</form>
	</div>
 </div> <!-- /container -->
@endsection