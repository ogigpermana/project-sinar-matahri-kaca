@extends('layouts.front')
@section('content')
    
<section id="blog">
        <div class="blog container">
            <div class="row" style="margin-top:100px;">
                <div class="col-md-8">
                    @if ($term = request('term'))
                    <div class="alert alert-info alert-dismissible fade in" role="alert"> 
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button> 
                        <strong><i class="fa fa-info-circle"></i></strong> Hasil pencarian dari: <strong>{{ $term }}</strong>. 
                    </div>
                    @endif
                    @forelse ($posts as $post)
                    <div class="blog-item">
                        @if ($post->image_url)
                        <img class="img-responsive img-blog" src="{{ $post->image_url }}" alt="" width="100%">
                        @else
                        <img class="img-responsive img-blog" src="/frontend/images/about.jpg" alt="" width="100%">
                        @endif
                        <div class="blog-content">
                                @php
                                    $commentsNumber = $post->comments->count()
                                @endphp
                            <span>
                                <i class="fa fa-user"></i> <a href="{{ route('user', $post->user->slug) }}">{{ $post->user->name }}</a> <i class="fa fa-angle-double-right"></i>
                                <i class="fa fa-folder"></i> <a href="{{ route('category', $post->category->slug) }}" class="blog_cat">{{ $post->category->title }}</a> <i class="fa fa-angle-double-right"></i>
                                <i class="fa fa-calendar"></i> <a href="#">{{ $post->date }}</a><i class="fa fa-angle-double-right"></i>
                                <i class="fa fa-comment"></i> <a href="{{ route('post.show', $post->slug) }}#post-comments">{{ $commentsNumber }} {{ str_plural('Comment', $commentsNumber) }}</a>
                            </span>
                            <h2><a href="{{ route('post.show', $post->slug) }}">{{ $post->title }}</a></h2>
                            <h3>{!! $post->excerpt !!}</h3>
                            <a class="readmore" href="{{ route('post.show', $post->slug) }}">Read More <i class="fa fa-long-arrow-alt-right"></i></a>
                        </div>    
                    </div>
                    @empty
                    <div class="alert alert-info alert-dismissible fade in" role="alert"> 
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button> 
                        <strong><i class="fa fa-info-circle"></i></strong> Tidak ada artikel yang dapat ditampilkan saat ini. 
                    </div>
                    @endforelse
                    <!--/.blog-item-->
                    <nav class="text-center">
                        {!! $posts->appends(request()->only(['term', 'moth', 'year']))->links() !!}
                    </nav>
                </div>
                <!--/.col-md-8-->

                <!--Sidebar -->
                @include('client.widgets.sidebar')
                
            </div>
            <!--/.row-->
        </div>
    </section>
    <!--/#blog-->
@endsection