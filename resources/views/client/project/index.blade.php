@forelse ($projects as $project)
<div class="col-md-4">
    <a href="{{ $project->image_url }}" class="smk-card-item image-popup" title="{{ $project->title }}">
        <figure>
            <div class="overlay"><i class="ti-plus"></i></div>
            <img src="{{ $project->image_thumb_url }}" alt="Image" class="img-responsive">
        </figure>
    </a>
</div>
@empty
<div class="alert alert-info alert-dismissible fade in" role="alert"> 
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button> 
    <strong><i class="fa fa-info-circle"></i></strong> Gallery project belum tersedia. 
</div>
@endforelse
