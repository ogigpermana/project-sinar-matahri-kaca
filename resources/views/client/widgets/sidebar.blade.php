<aside class="col-md-4">
        <div class="widget search">
            <form action="{{ route('blog') }}" value="{{ request('term') }}" role="form">
                <input type="text" name="term" id="search" class="form-control search_box" autocomplete="off" placeholder="Search Here" alue="{{ request('term') }}">
                <button type="submit"><i class="fa fa-search"></i></button>
            </form>
        </div>
        <!--/.search-->


        <div class="widget archieve">
            <h3>Kategori Produk</h3>
            <div class="row">
                <div class="col-sm-12">
                    <ul class="blog_archieve">
                        @forelse ($categories as $category)    
                        <li><a href="{{ route('category', $category->slug)}}"> {{ $category->title }} <span class="pull-right">({{ $category->posts->count() }})</span></a></li>
                        @empty
                        <div class="alert alert-info alert-dismissible fade in" role="alert"> 
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button> 
                            <strong><i class="fa fa-info-circle"></i></strong> Kategori belum tersedia. 
                        </div>
                        @endforelse
                    </ul>
                </div>
            </div>
        </div>
        <!--/.categories-->

        <div class="widget archieve">
            <h3>Pos Arsip</h3>
            <div class="row">
                <div class="col-sm-12">
                    <ul class="blog_archieve">
                        @forelse ($archives as $archive)
                        <li><a href="{{ route('blog', ['month' => $archive->month, 'year' => $archive->year]) }}"> {{ $archive->month . "/" . $archive->year }} <span class="pull-right">({{ $archive->post_count }})</span></a></li>
                        @empty
                        <div class="alert alert-info alert-dismissible fade in" role="alert"> 
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button> 
                            <strong><i class="fa fa-info-circle"></i></strong> Pos arsip tidak tersedia. 
                        </div>
                        @endforelse
                    </ul>
                </div>
            </div>
        </div>
        <!--/.archieve-->

        <div class="widget popular_post">
            <h3>Populer Artikel</h3>
            <ul>
            @forelse ($popularProducts as $product)
                <li>
                    <a href="{{ route('post.show', $product->slug) }}">
                        @if ($product->image_url)    
                            <img src="{{ $product->image_url }}" alt="{{ $product->title }}" width="100%" height="149">
                        @else
                            <img src="/frontend/images/about.jpg" alt="{{ $product->title }}" width="100%" height="149">
                        @endif
                        <p>{{ $product->title }}</p>
                    </a>
                </li>
            @empty
            <div class="alert alert-info alert-dismissible fade in" role="alert"> 
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button> 
                <strong><i class="fa fa-info-circle"></i></strong> Populer produk belum tersedia. 
            </div>  
            @endforelse
            </ul>
        </div>
        <!--/.archieve-->
        <div class="widget archieve">
            <h3>Tags</h3>
            <div class="row">
                <div class="col-sm-12">
                    <div class="tags">
                        @forelse ($tags as $tag)
                          <a href="{{ route('tag', $tag->slug) }}">{{$tag->name}}</a>
                        @empty
                          <a href="#">Data tidak ada.</a>                            
                        @endforelse
                    </div>
                </div>
            </div>
        </div>

    </aside>