<div class="comments" id="post-comments">
    @php
        $commentsNumber = $post->comments->count()
    @endphp
    <h2><i class="fa fa-comment"></i> {{ $commentsNumber }} {{ str_plural('Comment', $commentsNumber) }}</h2>
    @foreach ($postComments as $comment)
    <div class="single-comment">
        <div class="comment-img">
            <img class="comment-img" src="{{ $comment->avatar() }}" alt="{{ $comment->user_name }}">
        </div>
        <div class="comment-content">
            <h4>{{ $comment->user_name }} <small>{{ $comment->date }}</small></h4>
            <p>{!! $comment->body_html !!}</p>
            <p>
                @if ($comment->user_site_url)
                <strong><small>{{ $comment->user_site_url }}</small></strong>
                @endif
            </p>
        </div>
    </div>
    @endforeach
    <nav>
        {!! $postComments->links() !!}
    </nav>
    <hr >
    <div class="single-comment">
        <h2>Tinggalkan Komentar</h2>
        <div class="comment-content comment-form">
                @if (session('message'))
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ session('message') }}
                </div>
                @endif
                {!! Form::open(['route' => ['post.comments', $post->slug]]) !!}
                    @csrf
                        <!-- text input -->
                    <div class="form-group {{ $errors->has('user_name') ? ' is-invalid' : '' }}">
                        <label>Nama*</label>
                        {!! Form::text('user_name', null, ['class' => 'form-control']) !!}
                        @if ($errors->has('user_name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('user_name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('user_email') ? ' is-invalid' : '' }}">
                        <label>Email*</label>
                        {!! Form::email('user_email', null, ['class' => 'form-control']) !!}
                        @if ($errors->has('user_name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('user_email') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label>Alamat website (optional)</label>
                        {!! Form::text('user_site_url', null, ['class' => 'form-control']) !!}
                    </div>
    
                    <!-- textarea -->
                    <div class="form-group {{ $errors->has('body') ? ' is-invalid' : '' }}">
                        <label>Komentar*</label>
                        {!! Form::textarea('body', null, ['row' => 6, 'class' => 'form-control']) !!}
                        @if ($errors->has('body'))
                            <span class="help-block">
                                <strong>{{ $errors->first('body') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <button class="btn btn-primary" type="submit">Kirim komentar</button>
                    </div>
    
                {!! Form::close() !!}
        </div>
    </div>
</div>