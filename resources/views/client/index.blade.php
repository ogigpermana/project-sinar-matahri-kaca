@extends('layouts.front')
@section('content')
<div id="smk-hero" class="js-fullheight"  data-section="home">
	<div class="flexslider js-fullheight">
		<ul class="slides">
		   <li style="background-image: url(/frontend/images/slide_1.jpeg);">
			   <div class="overlay"></div>
			   <div class="container">
				   <div class="col-md-10 col-md-offset-1 text-center js-fullheight slider-text">
					   <div class="slider-text-inner">
						   <h2>Produksi Etalase.</h2>
						   <p>
								<a href="https://bit.ly/2O9mrkV" class="btn btn-success btn-lg"><i class="fab fa-whatsapp"></i> Direct Whatsapp</a>
								<a href="{{ route('contact') }}" class="btn btn-danger btn-lg"><i class="fas fa-paper-plane"></i> Direct Email</a>
						   </p>
					   </div>
				   </div>
			   </div>
		   </li>
		   <li style="background-image: url(/frontend/images/slide_2.jpeg);">
			   <div class="overlay"></div>
			   <div class="container">
				   <div class="col-md-10 col-md-offset-1 text-center js-fullheight slider-text">
					   <div class="slider-text-inner">
						   <h2>Building Accessories.</h2>
						   <p style="margin-top:80px;">
							 <a href="https://bit.ly/2O9mrkV" class="btn btn-success btn-lg"><i class="fab fa-whatsapp"></i> Direct Whatsapp</a>
							 <a href="{{ route('contact') }}" class="btn btn-danger btn-lg"><i class="fas fa-paper-plane"></i> Direct Email</a>
						   </p>
					   </div>
				   </div>
			   </div>
		   </li>
		   <li style="background-image: url(/frontend/images/slide_3.jpeg);">
			   <div class="overlay"></div>
			   <div class="container">
				   <div class="col-md-10 col-md-offset-1 text-center js-fullheight slider-text">
					   <div class="slider-text-inner">
						   <h2>Produksi Jenis Kaca.</h2>
						   <p style="margin-top:80px;">
								<a href="https://bit.ly/2O9mrkV" class="btn btn-success btn-lg"><i class="fab fa-whatsapp"></i> Direct Whatsapp</a>
								<a href="{{ route('contact') }}" class="btn btn-danger btn-lg"><i class="fas fa-paper-plane"></i> Direct Email</a>
						   </p>
					   </div>
				   </div>
			   </div>
		   </li>
		  </ul>
	  </div>
</div>

<div class="smk-section-overflow">

	<div class="smk-section" id="smk-services" data-section="services">
		<div class="smk-container">
			<div class="row">
				<div class="col-md-12">
					<div class="smk-heading">
						<h2 class="smk-left">Services</h2>
						<p>Kami memproduksi dan instalasi berbagai macam jenis kaca, etalase, alumunium, besi dan aksesoris bangunan.</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="row">

						<div class="col-md-12">
							<div class="feature-left">
								<span class="icon">
									<i class="fas fa-building"></i>
								</span>
								<div class="feature-copy">
									<h3>Kaca Riben</h3>
									<p>Perakitan, Instalasi dan Pemasangan.</p>
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="feature-left">
								<span class="icon">
									<i class="fas fa-building"></i>
								</span>
								<div class="feature-copy">
									<h3>Kaca Flora</h3>
									<p>Perakitan, Instalasi dan Pemasangan.</p>
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="feature-left">
								<span class="icon">
									<i class="fas fa-building"></i>
								</span>
								<div class="feature-copy">
									<h3>Etalase</h3>
									<p>Perakitan, Instalasi dan Pemasangan.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6 animate-box" data-animate-effect="fadeIn">
					<div class="row">
						<div class="col-md-12">
							<div class="feature-left">
								<span class="icon">
									<i class="icon-cog"></i>
								</span>
								<div class="feature-copy">
									<h3>Alumunium</h3>
									<p>Perakitan, Instalasi dan Pemasangan.</p>
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="feature-left">
								<span class="icon">
									<i class="icon-cog"></i>
								</span>
								<div class="feature-copy">
									<h3>Besi</h3>
									<p>Perakitan, Instalasi dan Pemasangan.</p>
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="feature-left">
								<span class="icon">
									<i class="fas fa-campground"></i>
								</span>
								<div class="feature-copy">
									<h3>Aksesoris Bangunan</h3>
									<p>Perakitan, Instalasi dan Pemasangan.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="smk-section" id="smk-product" data-section="product">
		<div class="smk-container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center smk-heading">
					<h2>Product</h2>
					<p>Dibawah ini merupakan produk yang telah kami kerjakan sejauh ini, silahkan dilihat dan jika anda tertarik silahkan hubungi kami melalui whatsapp diatas</p>
				</div>
			</div>
			<div class="row">
				@include('client.project.index')
			</div>
		</div>
	</div>
	
	<div class="smk-section" id="smk-faq" data-section="faq">
		<div class="smk-container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center smk-heading">
					<h2>Frequently Ask Questions</h2>
					<p>Pertanyaan yang sering dipertanyakan oleh konsumen</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">

					<div class="smk-accordion">
						<div class="smk-accordion-heading">
							<div class="icon"><i class="icon-cross"></i></div>
							<h3>Perakitan Jenis Kaca?</h3>
						</div>
						<div class="smk-accordion-content">
							<div class="inner">
								<p>Kaca dirakit sesuai dengan pesanan dari konsumen sekaligus diantar dan di install dari toko jika memungkinkan</p>
							</div>
						</div>
					</div>

				</div>
				<div class="col-md-6">

					<div class="smk-accordion">
						<div class="smk-accordion-heading">
							<div class="icon"><i class="icon-cross"></i></div>
							<h3>Perakitan Jenis Alumunium dan Besi</h3>
						</div>
						<div class="smk-accordion-content">
							<div class="inner">
								<p>Alumunium dan besi dirakit dan di install jika memungkinkan dari toko langsung sesuai dengan spesifikasi permintaan dari konsumen. </p>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>

</div>

<div id="smk-blog" data-section="about">
	<div class="smk-container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2 smk-heading" style="margin-bottom: 5px; padding: 10px">
				<h2 class="text-center">About Us</h2>
				<p>{{ \setting('app_about') }}</p>
			</div>
			<div class="col-md-8 col-md-offset-2" style="float:left; margin-top:-20px; padding:10px">
				<address>
					Email: <a href="mailto:{{ \setting('from_email') }}">{{ \setting('from_email') }}</a>.<br> 
					Telepon: <a href="tel:{{ \setting('phone') }}">{{ \setting('phone') }}</a><br> 
					Whatsapp: <a href="https://bit.ly/2O9mrkV">{{ \setting('whatsapp') }}</a><br> 
					{{ \setting('address') }}
				</address>
			</div>
		</div>
	</div>
</div>

<div class="smk-section" id="smk-faq" data-section="blog">
	<div class="smk-container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2 text-center smk-heading">
				<h2>Recent Posts</h2>
				<p>Artikel tentang produk-produk yang kami buat dan bisa dijadikan sebagai referensi untuk anda jika berminta dengan produk yang kami buat.</p>
			</div>
		</div>
		<div class="row">
			@if (isset($categoryName))
			<div class="text-center">
				<h3>Produk: <strong>{{ $categoryName }}</strong></h3>
			</div>
			@endif

			@if (isset($userName))
			<div class="text-center">
				<h3>Author: <strong>{{ $userName }}</strong></h3>
			</div>
			@endif
			@forelse ($posts as $post)
			<div class="col-md-4" style="margin-top:20px;">
				<div class="card mb-4 box-shadow">
					@if($post->image_url)
					<img class="card-img-top" src="{{ $post->image_url }}" data-holder-rendered="true" width="100%" height="225" style="border-top-left-radius:5px; border-top-right-radius:5px;">
					@else
					<img class="card-img-top" src="/frontend/images/about.jpg" data-holder-rendered="true" width="100%" height="225" style="border-top-left-radius:5px; border-top-right-radius:5px;">
					@endif
					<div class="card-body" style="background-color:white; padding:10px">
						<span><i class="fa fa-user"></i> <a href="{{ route('user', $post->user->slug) }}"><small>{{ $post->user->name }}</small></a></span> |
						<span><i class="fa fa-calendar"></i> <small><a href="#">{{ $post->date }}</a></small></span> |
						<i class="fa fa-folder"></i> <span><a href="{{ route('category', $post->category->slug) }}"><small>{{ $post->category->title }}</small></a></span>
						<h4 class="card-text" style="color:#333; margin-top:15px"><strong><a href="{{ route('post.show', $post->slug) }}">{{ str_limit($post->title, 40) }}</a></strong></h4>
					</div>
				</div>
			</div>
			@empty
			<div class="alert alert-info alert-dismissible fade in" role="alert"> 
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button> 
				<strong><i class="fa fa-info-circle"></i></strong> Tidak ada artikel yang bisa ditampilkan saat ini. 
			</div>
			@endforelse
		</div>
	</div>
</div>

</div>
  
<div id="smk-contact" data-section="contact" class="smk-cover smk-cover-xs" style="background-image:url(/frontend/images/img_bg_1.jpg);">
<div class="overlay"></div>
<div class="smk-container">
	<div class="row text-center">
		<div class="display-t">
			<div class="display-tc">
				<div class="col-md-12">
					<h4 style="font-size:20px; color:whitesmoke;">Jika anda memiliki pertanyaan, silahkan email kami di <a href="mailto:{{ \setting('from_email') }}">{{ \setting('from_email') }}</a></h4>
					<a href="{{ route('contact') }}" class="btn btn-danger btn-lg" style="color: white;"><i class="fa fa-paper-plane"></i> Direct Email</a>
				</div>
			</div>
		</div>
	</div>
</div>
</div>	
@endsection

