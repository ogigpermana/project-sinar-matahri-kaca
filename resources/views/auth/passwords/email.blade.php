@extends('layouts.backend.app')
@section('title', 'Send request reset link')
@section('content')
<body class="hold-transition login-page">
<div class="login-box">
        <div class="login-logo">
          <a href="{{ route('welcome') }}"><b>Sinar Matahari</b>Kaca</a>
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body">
          <p class="login-box-msg">Reset Password</p>
      
          @if (session('status'))
          <div class="alert alert-success" role="alert">
              {{ session('status') }}
          </div>
      @endif

      <form method="POST" action="{{ route('password.email') }}">
          @csrf

          <div class="form-group has-feedback">
                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" autofocus>

                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
              <span class="fa fa-envelope form-control-feedback"></span>
            </div>

          <div class="form-group row mb-0">
              <div class="col-md-6 offset-md-4">
                  <button type="submit" class="btn btn-primary">
                      {{ __('Send Password Reset Link') }}
                  </button>
              </div>
          </div>
      </form>
      
        </div>
        <!-- /.login-box-body -->
      </div>
      <!-- /.login-box -->   
@endsection
