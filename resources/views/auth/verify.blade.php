@extends('layouts.backend.app')
@section('title', 'Verify email')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <section class="content-header">
          <h1>
            Verify email
            <small>Please verify email before continue</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('verification.notice') }}">Verify proccess</a></li>
            <li class="active">Verify email</li>
          </ol>
        
        </section>
        <section class="content">
            <div class="box box-info with-border">
                <div class="box-header">{{ __('Verify Your Email Address') }}</div>
                <div class="box-body">
                        @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                        </div>
                    @endif

                    {{ __('Before proceeding, please check your email for a verification link.') }}
                    {{ __('If you did not receive the email') }}, <a href="{{ route('verification.resend') }}">{{ __('click here to request another') }}</a>.
                </div>
            </div>
        </section>
    </div>
</div>
@endsection
