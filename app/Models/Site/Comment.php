<?php

namespace App\Models\Site;

use Illuminate\Database\Eloquent\Model;
use GrahamCampbell\Markdown\Facades\Markdown;

class Comment extends Model
{
    protected $fillable = ['user_name', 'user_email', 'body', 'post_id', 'user_site_url']; 

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    public function avatar()
    {
        $email = $this->user_email;
        $default = "http://www.accountingweb.co.uk/sites/all/modules/custom/sm_pp_user_profile/img/default-user.png";
        $size = 120;

        return "https://www.gravatar.com/avatar/" . md5( strtolower( trim( $email ) ) ) . "?d=" . urlencode( $default ) . "&s=" . $size;
    }

    // Aksesor untuk konten pada artikel
    public function getBodyHtmlAttribute()
    {
        return $this->body ? Markdown::convertToHtml(e($this->body)) : NULL;
    }

    public function getDateAttribute()
    {
        return is_null($this->created_at) ? '' : $this->created_at->toFormattedDateString();
    }
}
