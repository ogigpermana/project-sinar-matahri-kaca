<?php

namespace App\Models\Site;

use Carbon\Carbon;
use App\Models\Site\Tag;
use App\Models\Actor\User;
use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\Model;
use GrahamCampbell\Markdown\Facades\Markdown;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use SoftDeletes;

    protected $fillable = ['title', 'slug', 'excerpt', 'body', 'published_at', 'category_id', 'image'];
    protected $dates = ['published_at'];

    // Merelasikan model User dengan model Post
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    // Merelasikan model Category dengan model Post
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    // Merelasikan model Post dengan Tag
    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    // Merelasikan model post dengan Comment
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    // Aksesor untuk konten pada artikel
    public function getBodyHtmlAttribute()
    {
        return $this->body ? Markdown::convertToHtml(e($this->body)) : NULL;
    }

    // Aksesor untuk ringkasan pada artikel
    public function getExcerptHtmlAttribute()
    {
        return $this->excerpt ? Markdown::convertToHtml(e($this->excerpt)) : NULL;
    }

    // Format Tanggal Pada Status Kolom Tabel Pos
    public function dateFormatted($showTimes = false)
    {
        $format = "d/m/Y";

        if($showTimes)
        {
            $format = $format . " H:i:s";
        }
        return $this->created_at->format($format);
    }

    // Label Penanda status publikasi artikel
    public function publicationLabel()
    {
        if(!$this->published_at)
        {
            return '<span class="label label-warning label-rounded">Draft</span>';
        }
        elseif($this->published_at && $this->published_at->isFuture())
        {
            return '<span class="label label-info label-rounded">Terjadwal</span>';
        }
        else
        {
            return '<span class="label label-success label-rounded">Diterbitkan</span>';
        }
    }

    // Aksesor untuk memanipulasi link gambar 
    public function getImageUrlAttribute()
    {

        $imageUrl = "";

        if (!is_null($this->image))
        {
            $directory = Config::get('cms.image.directory');
            $imagePath = public_path() . "/{$directory}/" . $this->image;
            if (file_exists($imagePath)) $imageUrl = asset("{$directory}/" . $this->image);
        }

        return $imageUrl;
    }

    // Aksesor untuk memanipulasi link thumbnail gambar
    public function getImageThumbUrlAttribute()
    {

        $imageUrl = "";

        if (!is_null($this->image))
        {
            $directory = Config::get('cms.image.directory');
            $ext = substr(strrchr($this->image, '.'), 1);
            $thumbnail = str_replace(".{$ext}" , "_thumb.{$ext}", $this->image);
            $imagePath = public_path() . "/{$directory}/" . $thumbnail;
            if (file_exists($imagePath)) $imageUrl = asset("{$directory}/" . $thumbnail);
        }

        return $imageUrl;
    }

    public function getDateAttribute()
    {
        return is_null($this->published_at) ? '' : $this->published_at->toFormattedDateString();
    }

    // public function getPublishedAtAttribute($value)
    // {
    //     return $this->attributes['published_at'] = $value ?: NULL;
    // }

    public function scopeLatestFirst($query)
    {
        return $query->orderBy('created_at', 'desc');
    }

    public function scopePopular($query)
    {
        return $query->orderBy('view_count', 'desc');
    }

    public function scopePublished($query)
    {
        return $query->where('published_at', '<=', Carbon::now());
    }

    public function scopeScheduled($query)
    {
        return $query->where("published_at", ">" , Carbon::now());
    }

    public function scopeDraft($query)
    {
        return $query->whereNull("published_at");
    }

    // Scope untuk memfilter tulisan
    public function scopeFilter($query, $filter)
    {
        if (isset($filter['month']) && $month = $filter['month']) {
            $query->whereRaw('month(published_at) = ?', [Carbon::parse($month)->month]);
            // $query->whereMonth('published_at', $month)
        }

        if (isset($filter['year']) && $year = $filter['year']) {
            $query->whereRaw('year(published_at) = ?', [$year]);
            // $query->whereYear('published_at', $year)
        }

        if(isset($filter['term']) && $term = $filter['term'])
        {
            $query->where(function($q) use($term) {

                $q->whereHas('user', function($qr) use ($term) {
                    $qr->where('name', 'LIKE', "%{$term}%");
                });

                $q->whereHas('category', function($qr) use ($term) {
                    $qr->where('title', 'LIKE', "%{$term}%");
                });

                $q->where('title', 'LIKE', "%{$term}%");
                $q->orWhere('excerpt', 'LIKE', "%{$term}%");

            });
        }
    }

    public function createTags($tagString)
    {
        $tags = explode(",", $tagString);
        $tagIds = [];
        foreach ($tags as $tag)
        {
            $newTag = new Tag();
            $newTag = Tag::firstOrCreate(
                ['slug' => str_slug($tag)], 
                ['name' => trim($tag)]);
            $newTag->slug = str_slug($tag);
            $newTag->save();
            $tagIds[] = $newTag->id;
        }
        $this->tags()->detach();
        $this->tags()->attach($tagIds);
        // option two to change detach or attach
        // $this->tags()->sync($tagIds); uncomment this
    }

    public static function archives()
    {
        return static::selectRaw('count(id) as post_count, 
                                year(published_at) year, 
                                monthname(published_at) month')
                    ->published()
                    ->groupBy('year', 'month')
                    ->orderByRaw('min(published_at) desc')
                    ->get();
    }
}
