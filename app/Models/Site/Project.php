<?php

namespace App\Models\Site;

use App\Models\Actor\User;
use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = ['title', 'image', 'user_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    // Aksesor untuk memanipulasi link gambar 
    public function getImageUrlAttribute()
    {

        $imageUrl = "";

        if (!is_null($this->image))
        {
            $directory = Config::get('cms.image.directory');
            $imagePath = public_path() . "/{$directory}/" . $this->image;
            if (file_exists($imagePath)) $imageUrl = asset("{$directory}/" . $this->image);
        }

        return $imageUrl;
    }

    // Aksesor untuk memanipulasi link thumbnail gambar
    public function getImageThumbUrlAttribute()
    {

        $imageUrl = "";

        if (!is_null($this->image))
        {
            $directory = Config::get('cms.image.directory');
            $ext = substr(strrchr($this->image, '.'), 1);
            $thumbnail = str_replace(".{$ext}" , "_thumb.{$ext}", $this->image);
            $imagePath = public_path() . "/{$directory}/" . $thumbnail;
            if (file_exists($imagePath)) $imageUrl = asset("{$directory}/" . $thumbnail);
        }

        return $imageUrl;
    }


    public function scopeLatestFirst()
    {
        return $this->orderBy('created_at', 'desc');
    }
}
