<?php

namespace App\Models\Actor;

use App\Models\Site\Post;
use App\Models\Site\Project;
use Illuminate\Support\Facades\Config;
use Illuminate\Notifications\Notifiable;
use GrahamCampbell\Markdown\Facades\Markdown;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laratrust\Traits\LaratrustUserTrait;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;
    use LaratrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'slug', 'email_verified_at', 'position', 'bio'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // Merelasikan model Post dengan model User
    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    // Merelasikan model Project dengan model User
    public function projects()
    {
        return $this->hasMany(Project::class);
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function getBioHtmlAttribute($value)
    {
        return $this->bio ? Markdown::convertToHtml(e($this->bio)) : NULL;
    }

    public function avatar()
    {
        $email = $this->email;
        $default = "https://cdn.iconscout.com/icon/free/png-256/user-avatar-contact-portfolio-personal-portrait-profile-6-5623.png";
        $size = 120;

        return "https://www.gravatar.com/avatar/" . md5( strtolower( trim( $email ) ) ) . "?d=" . urlencode( $default ) . "&s=" . $size;
    }

    public function removePostImages()
    { 
            $uploadPath = public_path(Config::get('cms.image.directory'));
     
            if ($this->posts()->withTrashed()->count()) {
                foreach ($this->posts()->withTrashed() as $post) {
                    $image = $post->image;
     
                    if ( ! empty($image) ) { 
                        $imagePath     = $uploadPath . '/' . $image;                                                            
                        $ext           = substr(strrchr($image, '.'), 1);                                                             
                        $thumbnail     = str_replace(".{$ext}", "_thumb.{$ext}", $image);
                        $thumbnailPath = $uploadPath . '/' . $thumbnail;
        
                        if ( file_exists($imagePath) ) unlink($imagePath);
                        if ( file_exists($thumbnailPath) ) unlink($thumbnailPath);            
                    }
                }            
            }
    }
}
