<?php

namespace App\Http\Controllers\Site;

use App\Models\Site\Admin;
use App\Http\Controllers\Controller;
use App\Notifications\Site\InboxMessage;
use App\Http\Requests\Site\ContactFormRequest;

class ContactController extends Controller
{
    // Membuat form kontak
    public function show()
    {
        return view('client.contact.index');
    }

    public function mailToAdmin(ContactFormRequest $message, Admin $admin)
	{ 
        //send the admin an notification
		$admin->notify(new InboxMessage($message));
		// redirect the user back
		return redirect()->back()->with('message', 'Terimakasih sudah mengirimkan email kepada kami! Kami akan merespon pesan anda sesegera mungkin!');
	}
}
