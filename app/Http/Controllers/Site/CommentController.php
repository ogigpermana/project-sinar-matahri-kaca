<?php

namespace App\Http\Controllers\Site;

use App\Models\Site\Post;
use App\Models\Site\Comment;
use App\Http\Controllers\Controller;
use App\Http\Requests\Site\CommentStoreRequest;

class CommentController extends Controller
{
    public function store(Post $post, CommentStoreRequest $request)
    {
        $data = $request->all();
        $data['post_id'] = $post->id;

        Comment::create($data);
        // simple way to do the same thing with above command
        // $post->comments()->create($request->all());

        return redirect()->back()->with('message', 'Komen anda berhasil ditambahkan.');
    }
}
