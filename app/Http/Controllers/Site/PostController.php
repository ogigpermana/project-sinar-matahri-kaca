<?php

namespace App\Http\Controllers\Site;

use App\Models\Site\Tag;
use App\Models\Site\Post;
use App\Models\Actor\User;
use App\Models\Site\Category;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class PostController extends Controller
{
    protected $limit = 6;

    public function index()
    {
        // Untuk pengurutan tanggal bisa dilakukan dengan 3 cara
        // Menggunakan orderBy('created_at', 'desc')
        // Menggunakan latest()
        // Menggunakan scope contoh: latestFirst() di definisikan dalam class model Post
        $posts = Post::with('user', 'category', 'tags')
                    ->latestFirst()
                    ->published()
                    ->take($this->limit)
                    ->get();
                    
        return view("client.index", compact('posts'));
    }

    public function blog()
    {
        $posts = Post::with('user', 'category', 'tags', 'comments')
        ->latestFirst()
        ->published()
        ->filter(request()->only(['term', 'year', 'month']))
        ->simplePaginate($this->limit);
        
        return view("client.blog.index", compact('posts'));
    }

    public function category(Category $category)
    {
        $categoryName = $category->title;
        $posts = $category->posts()
                          ->with('user', 'tags', 'comments')
                          ->latestFirst()
                          ->published()
                          ->paginate($this->limit);

        return view("client.index", compact('posts', 'categoryName'));
    }

    public function tag(Tag $tag)
    {
        $tagName = $tag->name;

        $posts = $tag->posts()
                        ->with('user', 'category')
                        ->latestFirst()
                        ->published()
                        ->paginate($this->limit);

        return view('client.blog.index', compact('posts', 'tagName'));
    }

    // Menampilkan detail artikel
    public function show(Post $post)
    {
        $ids = Session::get('ids') ? Session::get('ids') : [];
        if (!in_array($post->id, $ids)) 
        {
            $post->increment('view_count');
            $ids[] = $post->id;
            Session::put('ids', $ids);
        }

        $postComments = $post->comments()->simplePaginate(3);
        
        return view("client.show", compact('post', 'postComments'));
    }

    //
    public function user(User $user)
    {
        $userName = $user->name;
        $posts = $user->posts()
                          ->with('category', 'tags', 'comments')
                          ->latestFirst()
                          ->published()
                          ->paginate($this->limit);

        return view("client.index", compact('posts', 'userName'));   
    }
}
