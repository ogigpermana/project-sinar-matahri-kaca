<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;

class ProjectController extends Controller
{
    public function index()
    {
        return view("client.project.index");
    }
}
