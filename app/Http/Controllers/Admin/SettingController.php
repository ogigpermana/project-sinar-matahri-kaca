<?php

namespace App\Http\Controllers\Admin;

use App\Models\Site\Setting;
use Illuminate\Http\Request;

class SettingController extends BaseController
{
    public function index()
    {
        return view('backend.setting.index');
    }

    public function store(Request $request)
    {
        $rules = Setting::getValidationRules();
        $data = $this->validate($request, $rules);

        $validSettings = array_keys($rules);

        foreach ($data as $key => $val) {
            if( in_array($key, $validSettings) ) {
                Setting::add($key, $val, Setting::getDataType($key));
            }
        }

        return redirect()->back()->with('status', 'Settings has been saved.');
    }
}
