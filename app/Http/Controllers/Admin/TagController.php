<?php

namespace App\Http\Controllers\Admin;

use App\Models\Site\Tag;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Admin\BaseController;

class TagController extends BaseController
{
    protected $limit = 5;
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tagsCount = Tag::count();
        $tags = DB::table("tags")->paginate($this->limit);
        return view('backend.tag.index',compact('tags', 'tagsCount'));
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    	DB::table("tags")->delete($id);
    	return redirect()->back()->with('message', 'Item berhasil dihapus');
    }
}
