<?php

namespace App\Http\Controllers\Admin;

use App\Models\Site\Post;
use App\Models\Actor\User;
use App\Models\Site\Comment;
use App\Models\Site\Setting;
use Illuminate\Http\Request;
use App\Http\Requests\User\ProfileRequest;

class HomeController extends BaseController
{
    protected $limit = 4;
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $posts = Post::with('user')
        ->latestFirst()
        ->published()
        ->paginate($this->limit);

        $settings = Setting::all();

        $postsCount = Post::published()->count();

        $commentsCount = Comment::count();

        $visitorCount = Post::orderBy('view_count')->count();

        $usersCount = User::count();

        return view('backend.home.index', compact('settings', 'posts', 'postsCount', 'commentsCount', 'usersCount', 'visitorCount'));
    }

    public function imageManager()
    {
        return view('backend.imagemanager.index');
    }

    public function edit(Request $request)
    {
        $user = $request->user();

        return view('backend.home.edit', compact('user'));
    }

    public function update(ProfileRequest $request)
    {
        $user = $request->user();
        $user->update($request->all());

        return redirect()->back()->with('message', 'Profile berhasil di update');
    }
}
