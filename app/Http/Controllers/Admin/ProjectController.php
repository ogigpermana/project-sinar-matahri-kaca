<?php

namespace App\Http\Controllers\Admin;

use App\Models\Site\Project;
use Illuminate\Support\Carbon;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Config;
use App\Http\Requests\Site\ProjectRequest;
use App\Http\Controllers\Admin\BaseController;

class ProjectController extends BaseController
{
    protected $limit = 5;
    protected $uploadPath;

    public function __construct()
    {   
        parent::__construct();
        $imageDir = Config::get('cms.image.directory');
        $this->uploadPath = public_path($imageDir);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projectsCount = Project::count();
        $projects = Project::with('user')->latestFirst()->paginate($this->limit);
        return view('backend.projectgallery.index', compact('projects', 'projectsCount'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Project $gallery)
    {
        return view('backend.projectgallery.create', compact('gallery'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProjectRequest $request)
    {
        $data = $this->handleRequest($request);
        $request->user()->projects()->create($data);

        return redirect(route('gallery.index'))->with('message', 'Item gallery berhasil di tambahkan.');
    }

    private function handleRequest($request)
    {
        $data = $request->all();

        if ($request->hasFile('image')) 
        {
            $image = $request->file('image');
            $fileName = 'landingcasts-' . Carbon::now()->format('Y-m-dH:i:s') . '.' . $image->getClientOriginalName();
            $destination = $this->uploadPath;

            $successUploaded = $image->move($destination, $fileName);

            if ($successUploaded) {
                $width = Config::get('cms.image.thumbnail.width');
                $height = Config::get('cms.image.thumbnail.height');
                $extension = $image->getClientOriginalExtension();
                $thumbnail = str_replace(".{$extension}", "_thumb.{$extension}", $fileName);
                
                Image::make($destination . '/' . $fileName)
                    ->resize($width, $height)
                    ->save($destination . '/' . $thumbnail);
            }

            $data['image'] = $fileName;
        }

        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $gallery = Project::findOrFail($id);

        return view('backend.projectgallery.edit', compact('gallery'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProjectRequest $request, $id)
    {
        $gallery = Project::findOrFail($id);
        $oldImage = $gallery->image;
        $data = $this->handleRequest($request);
        $gallery->update($data);

        if ($oldImage !== $gallery->image) {
            $this->removeImage($oldImage);
        }

        return redirect()->route('gallery.index')->with('message', 'Project Image berhasil di update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $gallery = Project::findOrFail($id);
        $gallery->delete();

        $this->removeImage($gallery->image);

        return redirect()->back()->with('message', 'Artikel berhasil dihapus secara');
    }

    private function removeImage($image)
    {
        if(!empty($image))
        {
            $imagePath = $this->uploadPath . '/' . $image;
            $ext = substr(strrchr($image, '.'), 1);
            $thumbnail = str_replace(".{$ext}", "_thumb.{$ext}", $image);
            $thumbnailPath = $this->uploadPath . '/' . $thumbnail;

            if (file_exists($imagePath)) {
                unlink($imagePath);
            }

            if (file_exists($thumbnailPath)) {
                unlink($thumbnailPath);
            }
        }
    }
}
