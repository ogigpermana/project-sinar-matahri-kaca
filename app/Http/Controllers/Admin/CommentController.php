<?php

namespace App\Http\Controllers\Admin;

use App\Models\Site\Comment;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Admin\BaseController;

class CommentController extends BaseController
{
    protected $limit = 5;
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $commentsCount = Comment::count();
        $comments = DB::table("comments")->paginate($this->limit);
        return view('backend.comment.index',compact('comments', 'commentsCount'));
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    	DB::table("comments")->delete($id);
    	return redirect()->back()->with('message', 'Item berhasil dihapus');
    }
}
