<?php

use App\Models\Site\Post;

function check_user_permissions($request, $actionName = NULL, $id = NULL)
{
  // Get current user
  $currentUser = $request->user();
  
  // current action name
  if ($actionName) {
    $currentActionName = $actionName;
  }else{
    $currentActionName = $request->route()->getActionName();
  }

  list($controller, $method) = explode('@', $currentActionName);

  $controller = str_replace(["App\\Http\\Controllers\\Admin\\", "Controller"], "", $controller);


  $crudPermissionsMap = [
      // 'create' => ['create', 'store'],
      // 'update' => ['edit', 'update'],
      // 'delete' => ['destroy', 'restore', 'forceDestroy'],
      // 'read' => ['index', 'view']
      'crud' => ['create', 'store', 'edit', 'update', 'destroy', 'restore', 'forceDestroy', 'index', 'view']
  ];

  $classesMap = [
      'Post' => 'post',
      'Category' => 'category',
      'User' => 'user',
      'Project' => 'project',
      'Setting' => 'settings'
  ];

  foreach($crudPermissionsMap as $permission => $methods)
  {
      // If the current method exists in method list
      // we will check the permission
      if (in_array($method, $methods) && isset($classesMap[$controller])) 
      {
          $className = $classesMap[$controller];
          // Check permission to other posts
          if($className == 'post' && in_array($method, ['edit', 'update', 'destroy', 'restore', 'forceDestroy']))
          {
            $id = !is_null($id) ?  $id : $request->route("blog");
              // If the current user has not update-others-post/delete-others-post permission
              // make sure that he/she can only modified his/her own post
              if ($id && (!$currentUser->can('update-others-post') || !$currentUser->can('delete-others-post'))) 
              {
                  $post = Post::withTrashed()->find($id);
                  if ($post->user_id !== $currentUser->id) 
                  {
                      return false;
                  }
              }
          }
          // If the user has not permission do not allow next request
          elseif(!$currentUser->can("{$permission}-{$className}"))
          {
              return false;
          }
          break;
      }
  }

  return true;
}