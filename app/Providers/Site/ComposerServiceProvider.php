<?php

namespace App\Providers\Site;

use App\Models\Site\Tag;
use App\Models\Site\Post;
use App\Models\Site\Project;
use App\Models\Site\Category;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    protected $limit =3;
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('client.project.index', function($view){
            $projects = Project::with('user')->latestFirst()->get();

            return $view->with('projects', $projects);
        });

        view()->composer('client.widgets.sidebar', function($view){
            $popularProducts = Post::published()->popular()->take($this->limit)->get();

            return $view->with('popularProducts', $popularProducts);
        });

        view()->composer('client.widgets.sidebar', function($view){
            $categories = Category::with(['posts' => function($query) {
                $query->published();
            }])->orderBy('title', 'asc')->get();

            return $view->with('categories', $categories);
        });

        view()->composer('client.widgets.sidebar', function($view){
            $archives = Post::archives();

            return $view->with('archives', $archives);
        });

        view()->composer('client.widgets.sidebar', function($view){
            $tags = Tag::has('posts')->get();

            return $view->with('tags', $tags);
        });

        view()->composer('client.widgets.sidebar', function($view){
            $archives = Post::archives();

            return $view->with('archives', $archives);
        });
    }
}
