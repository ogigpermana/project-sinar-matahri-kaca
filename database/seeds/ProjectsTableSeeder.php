<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class ProjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // kosongkan tabel user
        DB::table('projects')->truncate();

        // Menambahkan data dummy pada tabel projects
        DB::table('projects')->insert([
            [
                'user_id' => rand(1,3),
                'title' => "Kaca Riben",
                'image' => 'produk_2.jpg',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'user_id' => rand(1,3),
                'title' => "Kaca Riben",
                'image' => 'produk_3.jpg',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'user_id' => rand(1,3),
                'title' => "Kaca Flora",
                'image' => 'produk_4.jpg',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'user_id' => rand(1,3),
                'title' => "Produk Kaca",
                'image' => 'produk_5.jpg',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'user_id' => rand(1,3),
                'title' => "Asesoris Bangunan",
                'image' => 'produk_6.jpg',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'user_id' => rand(1,3),
                'title' => "Etalase",
                'image' => 'produk_7.jpg',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
        ]);

    }
}
