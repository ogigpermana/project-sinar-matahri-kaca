<?php

use App\Models\Actor\User;
use App\Models\Config\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Mengosongkan tabel roles
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('roles')->truncate();
        
        // Create Admin Role
        $admin = new Role();
        $admin->name = "admin";
        $admin->display_name = "Administrator";
        $admin->description  = 'Memiliki hak akses penuh terhadap sistem'; // optional
        $admin->save();

        // Create moderator Role
        $moderator = new Role();
        $moderator->name = "moderator";
        $moderator->display_name = "Moderator";
        $moderator->description  = 'Memiliki hak akses untuk mengelola kategori dan artikel'; // optional
        $moderator->save();

        // Create Author Role
        $author = new Role();
        $author->name = "author";
        $author->display_name = "Author";
        $author->description  = 'Memiliki hak akses untuk membuat dan mengedit artikel sendiri'; // optional
        $author->save(); 

        // Attach The Roles
        // User as admin
        $user1 = User::find(1);
        $user1->detachRole($admin);
        $user1->attachRole($admin);

        // User as moderator
        $user2 = User::find(2);
        $user2->detachRole($moderator);
        $user2->attachRole($moderator);

        // User as author
        $user3 = User::find(3);
        $user3->detachRole($author);
        $user3->attachRole($author);
    }
}
