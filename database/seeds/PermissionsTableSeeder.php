<?php

use App\Models\Config\Role;
use Illuminate\Database\Seeder;
use App\Models\Config\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Mengosongkan tabel permissions
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        // Truncate permissions table
        DB::table('permissions')->truncate();
        
        // Crud post
        $crudPost = new Permission();
        $crudPost->name = "crud-post";
        $crudPost->save();

        // Update others post
        $updateOthersPost = new Permission();
        $updateOthersPost->name = "update-others-post";
        $updateOthersPost->save();

        // Delete others post
        $deleteOthersPost = new Permission();
        $deleteOthersPost->name = "delete-others-post";
        $deleteOthersPost->save();

        // Crud Category
        $crudCategory = new Permission();
        $crudCategory->name = "crud-category";
        $crudCategory->save();
        
        // Crud User
        $crudUser = new Permission();
        $crudUser->name = "crud-user";
        $crudUser->save();

        // Crud Project
        $crudProject = new Permission();
        $crudProject->name = "crud-project";
        $crudProject->save();

        // Crud Settings Application
        $crudSettings = new Permission();
        $crudSettings->name = "crud-settings";
        $crudSettings->save();

        // Attach roles permission
        $admin  = Role::where('name', 'admin')->first();
        $moderator = Role::where('name', 'moderator')->first();
        $author = Role::where('name', 'author')->first();

        $admin->detachPermissions([$crudPost, $updateOthersPost, $deleteOthersPost, $crudCategory, $crudUser, $crudProject, $crudSettings]);        
        $admin->attachPermissions([$crudPost, $updateOthersPost, $deleteOthersPost, $crudCategory, $crudUser, $crudProject, $crudSettings]);

        $moderator->detachPermissions([$crudPost, $updateOthersPost, $deleteOthersPost, $crudCategory]);
        $moderator->attachPermissions([$crudPost, $updateOthersPost, $deleteOthersPost, $crudCategory]);

        $author->detachPermissions([$crudPost]);        
        $author->attachPermissions([$crudPost]);
    }
}
