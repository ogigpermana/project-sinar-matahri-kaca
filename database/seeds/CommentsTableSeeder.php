<?php

use Faker\Factory;
use App\Models\Site\Post;
use App\Models\Site\Comment;
use Illuminate\Database\Seeder;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker    = Factory::create();
        $comments = [];

        $posts = Post::published()->latest()->take(5)->get();
        foreach ($posts as $post)
        {
            for ($i = 1; $i <= rand(1, 10); $i++)
            {
                $commentDate = $post->published_at->modify("+{$i} hours");

                $comments[] = [
                    'user_name' => $faker->name,
                    'user_email' => $faker->email,
                    'user_site_url' => $faker->domainName,
                    'body' => $faker->paragraphs(rand(1, 5), true),
                    'post_id' => $post->id,
                    'created_at' => $commentDate,
                    'updated_at' => $commentDate,
                ];
            }
        }

        Comment::truncate();
        Comment::insert($comments);
    }
}
