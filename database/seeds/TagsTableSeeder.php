<?php

use App\Models\Site\Tag;
use App\Models\Site\Post;
use Illuminate\Database\Seeder;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Truncate the tags table
        DB::table('tags')->truncate();

        $besi = new Tag();
        $besi->name = "Besi";
        $besi->slug = "besi";
        $besi->save();

        $alumunium = new Tag();
        $alumunium->name = "Alumunium";
        $alumunium->slug = "alumunium";
        $alumunium->save();

        $etalase = new Tag();
        $etalase->name = "Etalase";
        $etalase->slug = "etalase";
        $etalase->save();

        $kaca = new Tag();
        $kaca->name = "Kaca";
        $kaca->slug = "kaca";
        $kaca->save();

        $tags = [
            $alumunium->id,
            $besi->id,
            $etalase->id,
            $kaca->id
        ];

        foreach( Post::all() as $post)
        {
            shuffle($tags);
            
            for($i = 0; $i < rand(0, count($tags)-1); $i++)
            {
                $post->tags()->detach($tags[$i]);
                $post->tags()->attach($tags[$i]);
            }
        }
    }
}
