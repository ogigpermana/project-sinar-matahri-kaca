<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Set foreign key check to 0
        DB::statement("SET FOREIGN_KEY_CHECKS=0");
        
        // kosongkan tabel user
        DB::table('users')->truncate();

        // membuat beberapa user
        DB::table('users')->insert([
            [
                'name' => "Dian Ekawati",
                'slug' => "dian-ekawati",
                'email' => "sinarmatahari.kaca10@gmail.com",
                'email_verified_at' => Carbon::now(),
                'password' => bcrypt('secret'),
                'position' => "Owner and Business Women",
                'bio' => 'Seorang business women pengusaha sekaligus marketing'
            ],
            [
                'name' => "Operator Satu",
                'slug' => "operator-satu",
                'email' => "operator01@example.com",
                'email_verified_at' => Carbon::now(),
                'password' => bcrypt('secret'),
                'position' => "Operator",
                'bio' => 'I am moderator'
            ],
            [
                'name' => "Operator Dua",
                'slug' => "operator-dua",
                'email' => "operator02@example.com",
                'email_verified_at' => Carbon::now(),
                'password' => bcrypt('secret'),
                'position' => "Operator",
                'bio' => 'I am moderator'
            ]
        ]);
    }
}
