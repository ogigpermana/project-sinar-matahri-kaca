<?php

use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Kosongkan tabel posts
        DB::table('posts')->truncate();

        // Membuat data dummy untuk tabel posts
        $posts = [];
        $faker = Factory::create();
        $date = Carbon::create(2019, 3, 10, 11);

        for ($i = 1; $i <= 10; $i++) {
            $image = "produk_" . rand(2, 7) . ".jpg";
            // $date = date("Y-m-d H:i:s", strtotime("2019-03-11 14:00:00 + {$i} days"));
            $date->addDays(1);
            $publishedDate = clone($date);
            $createdDtae = clone($date);

            $posts[] = [
                'user_id' => rand(1, 3),
                'category_id' => rand(1,4),
                'title' => $faker->sentence(rand(8, 12)),
                'slug' => $faker->slug(),
                'excerpt' => $faker->text(rand(200, 300)),
                'body' => $faker->paragraphs(rand(10, 15), true),
                'image' => rand(0, 1) == 1 ? $image : NULL,
                'created_at' => $createdDtae,
                'updated_at' => $createdDtae,
                'published_at' => $i < 5 ? $publishedDate : (rand(0,1) == 0 ? NULL : $publishedDate->addDays(4)),
                'view_count' => rand(1, 10) * 10
            ];
        }

        DB::table('posts')->insert($posts);
    }
}
