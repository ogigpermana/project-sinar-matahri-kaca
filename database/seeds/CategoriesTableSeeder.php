<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // mengosongkan tabel kategori
        DB::table('categories')->truncate();

        // Manambahkan data dummy untuk tabel kategori
        DB::table('categories')->insert([
            [
                'title' => "Kaca",
                'slug' => "kaca"
            ],
            [
                'title' => "Besi",
                'slug' => "besi"
            ],
            [
                'title' => "Alumunium",
                'slug' => "alumunium"
            ],
            [
                'title' => "Etalase",
                'slug' => "etalase"
            ],
        ]);

        // melakukan update data pada tabel posts
        // for ($post_id =1; $post_id <= 10; $post_id++)
        // {

        //     $category_id = rand(1, 4);

        //     DB::table('posts')->where('id', $post_id)->update(['category_id' => $category_id]);
        // }
    }
}
