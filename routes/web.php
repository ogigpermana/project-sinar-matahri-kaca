<?php

Route::get('/', 'Site\PostController@index')->name('welcome');
Route::get('/blog', 'Site\PostController@blog')->name('blog');
Route::post('/blog/{post}/comments', 'Site\CommentController@store')->name('post.comments');
Route::get('/product/{category}', 'Site\PostController@category' )->name('category');
Route::get('/baca/{post}', 'Site\PostController@show')->name('post.show');
Route::get('/u/{user}', 'Site\PostController@user')->name('user');
Route::get('/t/{tag}', 'Site\PostController@tag')->name('tag');


Route::get('/contact', 'Site\ContactController@show')->name('contact');
Route::post('/contact',  'Site\ContactController@mailToAdmin');

Auth::routes(['verify' => true]);

Route::group(['middleware' => 'auth'], function () {
    Route::get('/settings', 'Admin\SettingController@index')->name('settings');
    Route::post('/settings', 'Admin\SettingController@store')->name('settings.store');
});

Route::get('/home', 'Admin\HomeController@index')->name('home');
// profile
Route::get('/home/edit-profile', 'Admin\HomeController@edit')->name('profile.edit');
Route::put('/home/edit-profile', 'Admin\HomeController@update');
Route::get('/profile', 'Admin\UserController@profile')->name('profile');

// image featur
Route::get('/home/imagemanager', 'Admin\HomeController@imageManager')->name('image.manager');
// Change password
Route::get('/change-password', 'Admin\UserController@showChangePasswordForm')->name('form.change.password');
Route::post('/change-password', 'Admin\UserController@changePassword')->name('change.password');
// User
Route::resource('/home/user', 'Admin\UserController');
Route::get('/home/user/confirm/{user}', 'Admin\UserController@confirm')->name('user.confirm');
// post
Route::resource('/home/blog', 'Admin\PostController');
Route::put('/home/blog/restore/{blog}', 'Admin\PostController@restore')->name('blog.restore');
Route::delete('/home/blog/force-destroy/{blog}', 'Admin\PostController@forceDestroy')->name('blog.force-destroy');
// Category
Route::resource('/home/category', 'Admin\CategoryController');
// Project Gallery
Route::resource('/home/gallery', 'Admin\ProjectController');
// Tags
Route::get('/home/tag', 'Admin\TagController@index')->name('tag.index');
Route::delete('/home/tag/{id}', 'Admin\TagController@destroy')->name('tag.destroy');
// Comments
Route::get('/home/comment', 'Admin\CommentController@index')->name('comment.index');
Route::delete('/home/comment/{id}', 'Admin\CommentController@destroy')->name('comment.destroy');
Route::get('phpinfo', function(){
    echo phpinfo();
});
