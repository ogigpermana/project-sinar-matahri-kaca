<?php
return [
    'image' => [
        'directory' => 'frontend/images',
        'thumbnail' => [
            'width' => 349.98,
            'height' => 225
        ]
    ],
    'default_category_id' => 1,
    'default_user_id' => 1
];