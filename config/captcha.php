<?php if (!class_exists('CaptchaConfiguration')) { return; } 

// BotDetect PHP Captcha configuration options 

return [ 
  // Captcha configuration for contact page 
  'ContactCaptcha' => [ 
    'UserInputID' => 'CaptchaCode', 
    'ImageWidth' => 250, 
    'ImageHeight' => 50, 
  ],
  'CommentCaptcha' => [ 
    'UserInputID' => 'CaptchaCode', 
    'ImageWidth' => 250, 
    'ImageHeight' => 50, 
  ],  

]; 